#!/bin/bash

# KIT-SR
# gdrive_download "1mwpPRH99EMphH7fdds11vSYOoC6umZA-" "KIT-SR.zip"
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1mwpPRH99EMphH7fdds11vSYOoC6umZA-' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1mwpPRH99EMphH7fdds11vSYOoC6umZA-" -O KIT-SR.zip
rm -rf /tmp/cookies.txt
unzip KIT-SR.zip -d KIT-SR/

# OSD
#gdrive_download "1_vNgQ2zrPVZHoD67BbhQspbbnInwdzw1" "OSD.zip"
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1_vNgQ2zrPVZHoD67BbhQspbbnInwdzw1' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1_vNgQ2zrPVZHoD67BbhQspbbnInwdzw1" -O OSD.zip
rm -rf /tmp/cookies.txt
unzip OSD.zip -d OSD/


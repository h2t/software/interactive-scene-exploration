#include "Platform.h"

namespace humanoid_robot_skills
{

Platform::Platform(std::shared_ptr<PlatformInterface> const& impl)
    : impl(impl)
{

}

Platform_MoveToLocation_Result Platform::moveToLocation(PlatformLocation targetLocation,
                                                        Platform_MoveToLocation_Params const& params)
{
    if (!impl)
    {
        throw std::runtime_error("PlatformImplementation is null in moveToLocation");
    }

    return impl->moveToLocation(targetLocation, params);
}

Platform_LookUpNamedLocation_Result Platform::lookUpNamedLocation(NamedPlatformLocation const& namedLocation)
{
    if (!impl)
    {
        throw std::runtime_error("PlatformImplementation is null in lookUpNamedLocation");
    }

    return impl->lookUpNamedLocation(namedLocation);
}

Platform_MoveToLocation_Result Platform::moveToNamedLocation(NamedPlatformLocation const& targetLocation,
                                                             Platform_MoveToLocation_Params const& params)
{
    if (!impl)
    {
        throw std::runtime_error("PlatformImplementation is null in moveToNamedLocation");
    }

    Platform_LookUpNamedLocation_Result resolved = impl->lookUpNamedLocation(targetLocation);
    if (!resolved.success)
    {
        Platform_MoveToLocation_Result result;
        result.success = false;
        return result;
    }

    return impl->moveToLocation(resolved.location, params);
}

PlatformLocation Platform::getCurrentLocation()
{
    if (!impl)
    {
        throw std::runtime_error("PlatformImplementation is null in getCurrentLocation");
    }

    return impl->getCurrentLocation();
}

}

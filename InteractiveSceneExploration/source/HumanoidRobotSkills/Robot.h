#pragma once

#include "API.h"

#include "Platform.h"

#include <string>
#include <map>

namespace humanoid_robot_skills
{

    using RobotConfig = std::map<std::string, float>;

    struct RobotState
    {
        PlatformLocation platformPose;
        RobotConfig jointAngles;
    };

    struct HRS_API RobotInterface
    {
        virtual ~RobotInterface() = default;

        virtual RobotState getState() = 0;
    };

    struct HRS_API Robot
    {
        RobotState getState();

        std::shared_ptr<RobotInterface> impl;
    };

}

#include "Robot.h"

namespace humanoid_robot_skills
{

    RobotState Robot::getState()
    {
        if (!impl)
        {
            throw std::runtime_error("RobotImplementation is null in getState");
        }

        return impl->getState();
    }

}

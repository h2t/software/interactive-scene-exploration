#pragma once

#include <InteractiveSceneExploration/API.h>
#include <InteractiveSceneExploration/ObjectHypothesis/ObjectHypothesis.h>

#include <SemanticObjectRelations/Shapes/shape_containers.h>

#include <Eigen/Core>

#include <memory>

namespace interactive_scene_exploration
{

    struct ISE_API SceneDistribution
    {
        std::vector<ObjectHypothesis> objects;

        ObjectHypothesis* findObjectWithID(int id);

        semrel::ShapeList sample(RandomEngine& randomEngine) const;

        semrel::ShapeList getMean() const;
    };



}

#pragma once

#include <InteractiveSceneExploration/API.h>
#include <InteractiveSceneExploration/PrimitiveFitting/ObjectFitter.h>
#include <InteractiveSceneExploration/ObjectHypothesis/SceneDistribution.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace interactive_scene_exploration
{
    using PointCloud = pcl::PointCloud<pcl::PointXYZRGBL>;

    struct PointCloudToSceneParams
    {
        ObjectFitterParams object;

        int normalEstimation_KSearch = 40;
    };

    struct PointCloudToSceneResult
    {
        SceneDistribution distribution;

        std::map<std::uint32_t, PointCloud::Ptr> segmentMap;

        std::vector<ObjectExtractionResult> extractionResults;
    };

    struct ISE_API PointCloudToScene
    {
        static PointCloudToSceneResult extractScene(
            PointCloudToSceneParams const& params,
            PointCloud const& pointCloud,
            RandomEngine& randomEngine);
    };


}

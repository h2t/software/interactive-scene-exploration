#include "SceneDistribution.h"

#include <cassert>

#include <VirtualRobot/math/Helpers.h>

namespace interactive_scene_exploration
{

    ObjectHypothesis* SceneDistribution::findObjectWithID(int id)
    {
        for (auto& object : objects)
        {
            if (object.id == id)
            {
                return &object;
            }
        }
        return nullptr;
    }

    semrel::ShapeList interactive_scene_exploration::SceneDistribution::sample(RandomEngine& randomEngine) const
    {
        semrel::ShapeList result;

        for (ObjectHypothesis const& obj : objects)
        {
            semrel::ShapePtr sampledObj(obj.sample(randomEngine));
            result.push_back(std::move(sampledObj));
        }

        return result;
    }

    semrel::ShapeList SceneDistribution::getMean() const
    {
        semrel::ShapeList result;

        for (ObjectHypothesis const& obj : objects)
        {
            semrel::ShapePtr meanObj(obj.getMeanShape());
            result.push_back(std::move(meanObj));
        }

        return result;
    }

}

#include "CylinderFitter.h"

#include <Eigen/Dense>

#include <boost/container/flat_set.hpp>

#include <cfloat>

#include <SimoxUtility/math/SoftMinMax.h>

#include <iostream>

namespace interactive_scene_exploration
{
    float pointToLineDistanceSquared(Eigen::Vector4f pt,
                                     Eigen::Vector4f line_pt,
                                     Eigen::Vector4f line_dir)
    {
        // Calculate the distance from the point to the line
        // D = ||(P2-P1) x (P1-P0)|| / ||P2-P1|| = norm (cross (p2-p1, p1-p0)) / norm(p2-p1)
        return (line_dir.cross3(line_pt - pt)).squaredNorm() / line_dir.squaredNorm();
    }

    float pointToLineDistanceSquared(Eigen::Vector3f pt,
                                     Eigen::Vector3f line_pt,
                                     Eigen::Vector3f line_dir)
    {
        // Calculate the distance from the point to the line
        // D = ||(P2-P1) x (P1-P0)|| / ||P2-P1|| = norm (cross (p2-p1, p1-p0)) / norm(p2-p1)
        return (line_dir.cross(line_pt - pt)).squaredNorm() / line_dir.squaredNorm();
    }

#if 1
    bool CylinderFitter::computeModelCoefficients(
        SampleMatrix const& points,
        SampleMatrix const& normals,
        ModelVector* modelCoefficients,
        EvaluatedCylinder* more)
    {
        Eigen::Vector4f p1(points.row(0).x(), points.row(0).y(), points.row(0).z(), 0.0);
        Eigen::Vector4f p2(points.row(1).x(), points.row(1).y(), points.row(1).z(), 0.0);

        Eigen::Vector4f n1(normals.row(0).x(), normals.row(0).y(), normals.row(0).z(), 0.0);
        Eigen::Vector4f n2(normals.row(1).x(), normals.row(1).y(), normals.row(1).z(), 0.0);

        Eigen::Vector4f w = n1 + p1 - p2;

        float a = n1.dot(n1); // Should be equal to 1.0
        float b = n1.dot(n2);
        float c = n2.dot(n2); // Should be equal to 1.0
        float d = n1.dot(w);
        float e = n2.dot(w);
        float denominator = a * c - b * b;
        float sc, tc;
        // Compute the line parameters of the two closest points
#if 0
        if (denominator < 1e-8)          // The lines are almost parallel
        {
            sc = 0.0f;
            tc = (b > c ? d / b : e / c);  // Use the largest denominator
        }
#else
        if (denominator < 1.0e-4)
        {
            return false;
        }
#endif
        else
        {
            sc = (b * e - c * d) / denominator;
            tc = (a * e - b * d) / denominator;
        }

        // point_on_axis, axis_direction
        Eigen::Vector4f line_pt  = p1 + n1 + sc * n1;
        if (!std::isfinite(line_pt.x()))
        {
            std::cerr << "p1: " << p1.transpose() << "\n"
                      << "n1: " << n1.transpose() << "\n"
                      << "sc: " << sc << "\n"
                      << "sc * n1" << (sc * n1) << "\n"
                      << "denominator: " << denominator << "\n"
                      << std::endl;
        }
        Eigen::Vector4f line_dir = p2 + tc * n2 - line_pt;
        line_dir.normalize();
        if (!std::isfinite(line_dir.x()))
        {
            std::cerr << "p2: " << p2.transpose() << "\n"
                      << "n2: " << n2.transpose() << "\n"
                      << "tc: " << tc << "\n"
                      << "tc * n2" << (tc * n2) << "\n"
                      << "denominator: " << denominator << "\n"
                      << std::endl;
        }
        float radius = std::sqrt(pointToLineDistanceSquared(p1, line_pt, line_dir));

        (*modelCoefficients).segment<3>(0) = line_pt.head<3>();
        (*modelCoefficients).segment<3>(3) = line_dir.head<3>();
        (*modelCoefficients)[6] = radius;

        more->p1 = p1.head<3>();
        more->n1 = n1.head<3>();
        more->p2 = p2.head<3>();
        more->n2 = n2.head<3>();
        more->pointL1 = line_pt.head<3>();
        more->pointL2 = (p2 + tc * n2).head<3>();
        more->direction = line_dir.head<3>();

        return true;
    }
#else
    // Try implementing it ourselves

    bool CylinderFitter::computeModelCoefficients(
        SampleMatrix const& points,
        SampleMatrix const& normals,
        ModelVector* modelCoefficients,
        EvaluatedCylinder* more)
    {
        // Line L1
        Eigen::Vector3f p1 = points.row(0);
        Eigen::Vector3f n1 = normals.row(0);

        // Line L2
        Eigen::Vector3f p2 = points.row(1);
        Eigen::Vector3f n2 = normals.row(1);

        // The minimal line segment L3 connecting L1 and L2 is perpendicular to both
        Eigen::Vector3f n3 = n2.cross(n1);
        // This is also the axis of the resulting cylinder!

        // The closest point on L1 is
        // c1 = p1 + t1 * n1 (with t1 unknown)
        // This defines L3
        // L3: c1 + t3 * n3 = p1 + t1 * n1 + t3 * n3

        // L3 also has a point on L2: L3(t3) = L2(t2)
        // p1 + t1 * n1 + t3 * n3 = p2 + t2 * n2
        // Rearranging gives
        // n1 * t1 - n2 * t2 + n3 * t3 = p2 - p1
        // Or in matrix form
        // |        |   | t1|
        // |n1 n2 n3| * |-t2| = p2 - p1
        // |        |   | t3|
        //     N      *   t   = p2 - p1
        Eigen::Matrix3f N;
        N.col(0) = n1;
        N.col(1) = n2;
        N.col(2) = n3;

        // Observe that N is an orthonormal matrix, i.e. N^T * N = I
        Eigen::Vector3f t = N.transpose() * (p2 - p1);

        float t1 = t(0);
        float t2 = -t(1);
        float t3 = t(2);

        // One point on the axis line is on L1 and another on L2
        Eigen::Vector3f pointL1 = p1 + t1 * n1;
        Eigen::Vector3f pointL2 = p2 + t2 * n2;

        // The cylinder axis is defined through a point and a direction
        //Eigen::Vector3f direction = pointL2 - pointL1;
        Eigen::Vector3f direction = n3;
        if (direction.norm() < 1.0e-3)
        {
            direction = Eigen::Vector3f::UnitZ();
        }
        else
        {
            direction.normalize();
        }


        if (!std::isfinite(pointL1.x()))
        {
            std::cerr << "p1: " << p1.transpose() << "\n"
                      << "n1: " << n1.transpose() << "\n"
                      << "sc: " << t1 << "\n"
                      << "sc * n1" << (t1 * n1) << "\n"
                      << std::endl;
        }
        if (!std::isfinite(direction.x()))
        {
            std::cerr << "p2: " << p2.transpose() << "\n"
                      << "n2: " << n2.transpose() << "\n"
                      << "tc: " << t2 << "\n"
                      << "tc * n2" << (t2 * n2) << "\n"
                      << std::endl;
        }
        float radius = std::sqrt(pointToLineDistanceSquared(p1, pointL1, direction));

        (*modelCoefficients).segment<3>(0) = pointL1.head<3>();
        (*modelCoefficients).segment<3>(3) = direction.head<3>();
        (*modelCoefficients)[6] = radius;

        more->p1 = p1;
        more->n1 = n1;
        more->p2 = p2;
        more->n2 = n2;
        more->pointL1 = pointL1;
        more->pointL2 = pointL2;
        more->direction = direction;

        return true;
    }
#endif

    void CylinderFitter::selectMSS(
        std::vector<Eigen::Vector3f> const& points,
        std::vector<Eigen::Vector3f> const& normals,
        RandomEngine& randomEngine,
        SampleMatrix* samplePoints,
        SampleMatrix* sampleNormals)
    {
        // FIXME: Should we remove this allocation by moving indices into an object?
        boost::container::flat_set<int> indices;
        indices.reserve(MSS_SIZE);

        std::uniform_int_distribution<int> dist(0, points.size() - 1);
        while ((int)indices.size() < MSS_SIZE)
        {
            // add a random index (is ignored if already contained)
            indices.insert(dist(randomEngine));
        }

        int row = 0;
        for (int index : indices)
        {
            (*samplePoints).row(row) = points[index];
            (*sampleNormals).row(row) = normals[index];
            ++row;
        }
    }

    std::vector<EvaluatedCylinder> CylinderFitter::sampleModels(
        const CylinderFitterParams& params,
        std::vector<Eigen::Vector3f> const& points,
        std::vector<Eigen::Vector3f> const& normals,
        RandomEngine& randomEngine)
    {
        std::vector<EvaluatedCylinder> result;

        if (points.size() < MSS_SIZE)
        {
            return result;
        }

        unsigned skipped_count = 0;
        // supress infinite loops by just allowing 10 x maximum allowed iterations for invalid model parameters!
        const unsigned max_skip = params.maxIterations * 10;

        result.reserve(points.size());


        std::vector<int> inlierIndices;
        for (int iterations_ = 0; iterations_ < params.maxIterations && skipped_count < max_skip; ++iterations_)
        {
            EvaluatedCylinder evaluated;

            // Get X samples which satisfy the model criteria
            SampleMatrix selection;
            SampleMatrix selectedNormals;
            selectMSS(points, normals, randomEngine, &selection, &selectedNormals);

            if (!std::isfinite(selectedNormals.row(0).x())
                || !std::isfinite(selectedNormals.row(1).x()))
            {
                --iterations_;
                ++skipped_count;
                continue;
            }

            // Search for inliers in the point cloud for the current plane model M
            ModelVector model_coefficients = ModelVector::Zero();
            bool hasModelCoefficients = computeModelCoefficients(selection, selectedNormals, &model_coefficients, &evaluated);
            if (!hasModelCoefficients)
            {
                --iterations_;
                ++skipped_count;
                continue;
            }

            // TODO: Replace with accessors defined in ObjectHypothesis
            //       But we need to move them somewhere central first
            Eigen::Vector3f position = model_coefficients.head<3>();
            Eigen::Vector3f direction = model_coefficients.block<3, 1>(3, 0);
            float radius = model_coefficients[6];

            //            std::cout << "[" << iterations_ << "] \n"
            //                      << "  Pos: " << position.transpose()
            //                      << "\n  Dir: " << direction.transpose()
            //                      << "\n R: " << radius << std::endl;

            // We do not have the height at this point. It will be figured out later!
            //float height = model_coefficients(8);
            if (radius > params.maxRadius)
            {
                ++skipped_count;
                continue;
            }

            if (!std::isfinite(position.x()))
            {
                std::cerr << "position is not finite: " << position.transpose() << "\n";
            }
            if (!std::isfinite(direction.x()))
            {
                std::cerr << "direction is not finite: " << direction.transpose() << "\n";
            }
            if (!std::isfinite(radius))
            {
                std::cerr << "radius is not finite: " << radius << "\n";
            }

            semrel::Cylinder cylinder(position, direction, radius);


            // Now, we have the model and need to calculate the inliers as well as the error
            Evaluation evaluation = evaluate(params, cylinder, points, &inlierIndices);
            float inlierRate = (float)inlierIndices.size() / points.size();
            if (inlierRate < params.minInlierRate)
            {
                continue;
            }
            if (inlierIndices.size() < 10)
            {
                continue;
            }
            {
                // compute height from inliers
                // project inliers onto cylinder axis and get their line parameter
                // take the min and max line parameter => top, bottom
                // recompute cylinder position

                simox::math::SoftMinMax tMinMax(params.outlierRate, inlierIndices.size());

                Eigen::ParametrizedLine<float, 3> axis = cylinder.getAxis();
                for (int i : inlierIndices)
                {
                    Eigen::Vector3f pointOnAxis = axis.projection(points[i]);
                    Eigen::Vector3f dir = axis.direction();
                    Eigen::Vector3f diff = pointOnAxis - axis.origin();
                    float t = diff.dot(dir);
                    tMinMax.add(t);
                    //                    std::cout << "t = " << t
                    //                              << ", dir: " << dir.transpose()
                    //                              << ", diff: " << diff.transpose()
                    //                              << ", or: " << points[i].transpose()
                    //                              << ", pr: " << pointOnAxis.transpose()
                    //                              << std::endl;
                }

                float minT = tMinMax.getSoftMin();
                float maxT = tMinMax.getSoftMax();

                Eigen::Vector3f bot = axis.pointAt(minT);
                Eigen::Vector3f top = axis.pointAt(maxT);

                // mid point is new position
                Eigen::Vector3f newPosition = 0.5 * (bot + top);

                // height is distance between top and bot
                float height = (bot - top).norm();

                cylinder.setHeight(height);
                cylinder.setPosition(newPosition);
            }

            evaluated.shape = cylinder;
            evaluated.evaluation = evaluation;
            result.push_back(evaluated);
        }

        return result;
    }

    static float CylinderNoHeightErrorFunction(Eigen::Vector3f point,
            semrel::Cylinder const& cylinder)
    {
        float distance = cylinder.getAxis().distance(point) - cylinder.getRadius();
        return distance * distance;
    }

    static float CylinderErrorFunction(Eigen::Vector3f point,
                                       semrel::Cylinder const& cylinder)
    {
        // source: http://liris.cnrs.fr/Documents/Liris-1297.pdf

        /*
         * Visualization:
         *
         *       Side (CASE 1) o p     Circle (CASE 3)
         *    __|______________:____|__
         *      |              :    |
         *    r |              : y  |  Disc (CASE 2)
         *      |           x  :    |
         * -----o---------o--->o----o-------- central axis
         *      a         c    h    b
         *
         * a = top, b = bottom
         * p = point, c = cylinder position
         * h = projected point
         * x = distance along axis, y = distance orthogonal to axis
         */

        // have to differentate 3 cases (shortest distance to mantle, disc, circle)

        // h in source
        Eigen::Vector3f pointOnAxis = cylinder.getAxis().projection(point);
        // x in source
        float distAlongAxis = (cylinder.getPosition() - pointOnAxis).norm();

        // y in source
        float distFromAxis = cylinder.getAxis().distance(point);

        float distToTube = distFromAxis - cylinder.getRadius();
        float distToDisc = distAlongAxis - cylinder.getHeight() / 2;

        float squaredDistance = 0;

        if (distAlongAxis <= cylinder.getHeight() / 2)
        {
            // SIDE or DISC: pointOnAxis inside cylinder
            float distance = std::abs(distToTube);
            if (distFromAxis <= cylinder.getRadius())
            {
                // inside cylinder => distance to disc could be shorter
                distance = std::min(distance, std::abs(distToDisc));
            }
            squaredDistance = distance * distance;
        }
        else if (distFromAxis <= cylinder.getRadius())
        {
            // DISC: projects onto disc
            squaredDistance = distToDisc * distToDisc;
        }
        else
        {
            // CIRCLE: projects onto circle => pythagoras
            squaredDistance = distToTube * distToTube + distToDisc * distToDisc;
        }

        return squaredDistance;
    }

    Evaluation CylinderFitter::evaluate(CylinderFitterParams const& params,
                                        semrel::Cylinder const& model,
                                        std::vector<Eigen::Vector3f> const& points,
                                        std::vector<int>* inlierIndices)
    {
        // allow some points to be ignored for error sum to cope with outliers
        std::size_t numInliers = 0;

        std::vector<float> errors;
        errors.reserve(points.size());
        simox::math::SoftMinMax errorSoftMax(params.outlierRate, points.size());

        float inlierErrorThreshold = params.distanceThreshold * params.distanceThreshold;
        inlierIndices->clear();
        inlierIndices->reserve(points.size());
        bool hasHeight = model.hasHeight();
        for (std::size_t i = 0; i < points.size(); ++i)
        {
            Eigen::Vector3f point = points[i];
            float pointError = hasHeight ? CylinderErrorFunction(point, model) : CylinderNoHeightErrorFunction(point, model);
            if (pointError <= inlierErrorThreshold)
            {
                inlierIndices->push_back(i);
                numInliers++;
            }
            errors.push_back(pointError);
            errorSoftMax.add(pointError);
        }

        float maxError = errorSoftMax.getSoftMax();

        // only add errors <= maxError
        float errorSum = 0;
        for (float pointError : errors)
        {
            errorSum += (pointError <= maxError ? pointError : 0);
        }

        float error = (numInliers > 0 ? errorSum / points.size() : std::numeric_limits<float>::max());
        // TODO: Dow we need the size penalty?
        //error += sizePenalty(model);

        float coverage = numInliers / float(points.size());

        Evaluation result;
        result.error = error;
        result.inlierCount = numInliers;
        result.coverage = coverage;
        return result;
    }

}

#pragma once

#include <InteractiveSceneExploration/ObjectHypothesis/ObjectHypothesis.h>
#include <InteractiveSceneExploration/PrimitiveFitting/BoxFitter.h>
#include <InteractiveSceneExploration/PrimitiveFitting/SphereFitter.h>
#include <InteractiveSceneExploration/PrimitiveFitting/CylinderFitter.h>

namespace interactive_scene_exploration
{

    struct ObjectFitterParams
    {
        BoxFitterParams box;
        CylinderFitterParams cylinder;
        SphereFitterParams sphere;

        float boltzmannTemperature = 0.05f;
    };

    struct ObjectExtractionResult
    {
        ObjectHypothesis object;

        std::vector<EvaluatedBox> boxes;
        std::vector<EvaluatedCylinder> cylinders;
        std::vector<EvaluatedSphere> spheres;
    };

    struct ObjectFitter
    {
        static ObjectExtractionResult extractObject(ObjectFitterParams const& params,
                int label,
                std::vector<Eigen::Vector3f> const& points,
                std::vector<Eigen::Vector3f> const& normals,
                RandomEngine& randomEngine);

        static std::vector<EvaluatedBox> extractBoxes(BoxFitterParams const& params,
                std::vector<Eigen::Vector3f> const& points,
                RandomEngine& randomEngine);

        static std::optional<BoxHypothesis> estimateBox(std::vector<EvaluatedBox> const& boxes);

        static std::optional<BoxHypothesis> estimateDistribution(std::vector<EvaluatedBox> const& boxes);


        static  std::vector<EvaluatedCylinder> extractCylinders(
            CylinderFitterParams const& params,
            std::vector<Eigen::Vector3f> const& points,
            std::vector<Eigen::Vector3f> const& normals,
            RandomEngine& randomEngine);

        static std::optional<CylinderHypothesis> estimateDistribution(std::vector<EvaluatedCylinder> const& models);

        static std::optional<CylinderHypothesis> estimateCylinder(std::vector<EvaluatedCylinder> const& samples);

        static std::vector<EvaluatedSphere> extractSpheres(
            SphereFitterParams const& params,
            std::vector<Eigen::Vector3f> const& points,
            RandomEngine& randomEngine);

        static std::optional<SphereHypothesis> estimateSphere(std::vector<EvaluatedSphere> const& spheres);

        static std::optional<SphereHypothesis> estimateDistribution(std::vector<EvaluatedSphere> const& spheres);
    };

}

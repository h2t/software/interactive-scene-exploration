#pragma once

#include "ShapeModels.h"

// TODO: Make a header for the random engine!
#include <InteractiveSceneExploration/Math/Gaussian.h>

#include <SemanticObjectRelations/Shapes/Sphere.h>

#include <Eigen/Core>

namespace interactive_scene_exploration
{



    struct SphereFitterParams
    {
        float distanceThreshold = 5.0f;
        int maxIterations = 100;
        float outlierRate = 0.01f;
        float maxRadius = 100.0f;
    };

    struct SphereFitter
    {
        static const int MSS_SIZE = 4;
        // [x, y, z, r]
        static const int MODEL_SIZE = 4;

        // Each row contains a sample
        using SampleMatrix = Eigen::Matrix<float, MSS_SIZE, 3>;

        using ModelVector = Eigen::Matrix<float, MODEL_SIZE, 1>;

        static bool computeModelCoefficients(SampleMatrix const& samples,
                                             ModelVector* modelCoefficients);

        static SampleMatrix selectMSS(std::vector<Eigen::Vector3f> const& points,
                                      RandomEngine& randomEngine);

        static std::vector<EvaluatedSphere> sampleModels(SphereFitterParams const& params,
                std::vector<Eigen::Vector3f> const& points,
                RandomEngine& randomEngine);

        static Evaluation evaluate(SphereFitterParams const& params,
                                   semrel::Sphere const& model,
                                   std::vector<Eigen::Vector3f> const& points);
    };

}

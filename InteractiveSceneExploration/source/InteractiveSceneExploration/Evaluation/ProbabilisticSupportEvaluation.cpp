#include "ProbabilisticSupportEvaluation.h"

#include <iostream>

#include <InteractiveSceneExploration/Math/Statistics.h>

namespace interactive_scene_exploration
{

    Relation probabilisticGraphToRelation(ProbabilisticSupportGraph const& graph)
    {
        Relation result;

        for (auto edge : graph.edges())
        {
            int source = (int)edge.sourceObjectID();
            int target = (int)edge.targetObjectID();

            double prob = edge.attrib().geometricExistenceProbability;
            result.setEntry(source, target, prob);
        }

        return result;
    }

    Relation supportGraphToRelation(semrel::SupportGraph const& graph)
    {
        Relation result;

        for (auto edge : graph.edges())
        {
            int source = (int)edge.sourceObjectID();
            int target = (int)edge.targetObjectID();

            result.setEntry(source, target, 1.0);
        }

        return result;
    }

    Relation groundTruthToRelation(std::vector<SupportRelation> const& gt)
    {
        Relation result;

        for (auto& rel : gt)
        {
            result.setEntry(rel.source, rel.target, 1.0);
        }

        return result;
    }


    std::vector<ExtractedRelations> ProbabilisticSupportEvaluation::extractRelations(
        ProbabilisticSupportEvaluationParams const& params,
        Dataset const& dataset,
        RandomEngine& randomEngine)
    {
        int entryCount = (int)dataset.entries.size();
        std::vector<ExtractedRelations> relations;
        relations.reserve(entryCount);

        std::cout << "Extracting relations from the " << dataset.id << std::endl;
        for (int i = 0; i < entryCount; ++i)
        {
            std::cout << "Working on entry " << i << std::endl;

            auto& entry = dataset.entries[i];

            PointCloudToSceneResult scene = PointCloudToScene::extractScene(params.scene, entry.pointCloud, randomEngine);

            ProbabilisticSupportAnalysisResult result = ProbabilisticSupportAnalysis::analyze(scene.distribution, params.support, randomEngine);

            ExtractedRelations rel;
            rel.groundTruth = groundTruthToRelation(entry.supportRelations);
            rel.probabilistic = probabilisticGraphToRelation(result.graph);
            rel.deterministic = supportGraphToRelation(result.deterministicSupportGraph);
            relations.push_back(rel);

            Relation cut = rel.probabilistic.binarize(0.5);
            double probBrier = Relation::brierScore(rel.probabilistic, rel.groundTruth);
            double cutBrier = Relation::brierScore(cut, rel.groundTruth);
            double suppBrier = Relation::brierScore(rel.deterministic, rel.groundTruth);
            std::cout << "Brier score: Supp " << suppBrier
                      << ", Prob: " << probBrier
                      << ", Cut: " << cutBrier
                      << std::endl;
        }

        return relations;
    }

    static void add(BrierScore& score, Relation const& predR, Relation const& gtR)
    {
        Relation all = predR.unionWith(gtR);
        for (auto& entry : all.entries)
        {
            double pred = predR.getEntry(entry.source, entry.target);
            double gt = gtR.getEntry(entry.source, entry.target);

            score.add(pred, gt);
        }
    }

    static void add(Accuracy& acc, Relation const& predR, Relation const& gtR)
    {
        Relation all = predR.unionWith(gtR);
        for (auto& entry : all.entries)
        {
            double pred = predR.getEntry(entry.source, entry.target);
            double gt = gtR.getEntry(entry.source, entry.target);

            acc.add(pred >= 0.5, gt >= 0.5);
        }
    }

    static ProbabilisticSupportEvaluationSingleResult createSingleResult(BrierScore const& brier, Accuracy const& acc)
    {
        ProbabilisticSupportEvaluationSingleResult result;

        result.brierScore = brier.get();
        result.accuracy = acc.accuracy();
        result.precision = acc.precision();
        result.recall = acc.recall();
        result.F1 = acc.F1();

        return result;
    }

    ProbabilisticSupportEvaluationResult ProbabilisticSupportEvaluation::evaluate(std::vector<ExtractedRelations> const& relations)
    {
        BrierScore brierDet;
        BrierScore brierProb;
        Accuracy accDet;
        Accuracy accProb;

        for (auto& rel : relations)
        {
            add(brierDet, rel.deterministic, rel.groundTruth);
            add(brierProb, rel.probabilistic, rel.groundTruth);

            add(accDet, rel.deterministic, rel.groundTruth);
            Relation cut = rel.probabilistic.binarize(0.5);
            add(accProb, cut, rel.groundTruth);
        }

        ProbabilisticSupportEvaluationResult result;

        result.deterministic = createSingleResult(brierDet, accDet);
        result.probabilistic = createSingleResult(brierProb, accProb);

        return result;
    }

    ProbabilisticSupportEvaluationResult ProbabilisticSupportEvaluation::evaluateOnDataset(
        ProbabilisticSupportEvaluationParams const& params,
        Dataset const& dataset,
        RandomEngine& randomEngine)
    {
        std::vector<ExtractedRelations> relations = extractRelations(params, dataset, randomEngine);

        ProbabilisticSupportEvaluationResult result = evaluate(relations);
        return result;
    }

    void ProbabilisticSupportEvaluation::evaluateDistribution(
            PointCloudToSceneParams const& params,
            Dataset const& dataset,
            RandomEngine& randomEngine)
    {
        int entryCount = (int)dataset.entries.size();
        std::vector<ExtractedRelations> relations;
        relations.reserve(entryCount);

        std::cout << "Extracting scenes from the " << dataset.id << std::endl;
        for (int i = 0; i < entryCount; ++i)
        {
            std::cout << "Working on entry " << i << std::endl;

            auto& entry = dataset.entries[i];

            PointCloudToSceneResult scene = PointCloudToScene::extractScene(params, entry.pointCloud, randomEngine);


            // Output the samples drawn so that we can make a multivariate normal test
            int index = 0;
            for (ObjectExtractionResult const& extractionResult : scene.extractionResults)
            {
                ++index;
                std::string indexStr = std::to_string(i) + "-" + std::to_string(index);

                std::ofstream outBox("samples-box-" + indexStr + ".csv");
                for (int i = 0; i < BoxHypothesis::STATE_SIZE; ++i)
                {
                    outBox << "x" << i << ";";
                }
                outBox << "\n";
                for (EvaluatedBox const& box : extractionResult.boxes)
                {
                    BoxHypothesis::State state = BoxHypothesis::toState(box.shape);
                    for (int i = 0; i < BoxHypothesis::STATE_SIZE; ++i)
                    {
                        outBox << state[i] << ";";
                    }
                    outBox << "\n";
                }

                std::ofstream outCyl("samples-cyl-" + indexStr + ".csv");
                for (int i = 0; i < CylinderHypothesis::STATE_SIZE; ++i)
                {
                    outCyl << "x" << i << ";";
                }
                outCyl << "\n";
                for (EvaluatedCylinder const& cyl : extractionResult.cylinders)
                {
                    CylinderHypothesis::State state = CylinderHypothesis::toState(cyl.shape);
                    for (int i = 0; i < CylinderHypothesis::STATE_SIZE; ++i)
                    {
                        outCyl << state[i] << ";";
                    }
                    outCyl << "\n";
                }

                std::ofstream outSph("samples-sph-" + indexStr + ".csv");
                for (int i = 0; i < SphereHypothesis::STATE_SIZE; ++i)
                {
                    outSph << "x" << i << ";";
                }
                outSph << "\n";
                for (EvaluatedSphere const& sph : extractionResult.spheres)
                {
                    SphereHypothesis::State state = SphereHypothesis::toState(sph.shape);
                    for (int i = 0; i < SphereHypothesis::STATE_SIZE; ++i)
                    {
                        outSph << state[i] << ";";
                    }
                    outSph << "\n";
                }
            }

        }
    }

}

#pragma once

#include <InteractiveSceneExploration/API.h>

#include <vector>

namespace interactive_scene_exploration
{
    struct ISE_API BrierScore
    {
        void add(double pred, double gt);

        double get() const;

        double sum = 0.0;
        int count = 0;
    };

    struct ISE_API Accuracy
    {
        void add(bool pred, bool gt);

        double precision() const;
        double recall() const;
        double accuracy() const;
        double F1() const;

        int truePositive = 0;
        int trueNegative = 0;
        int falsePositive = 0;
        int falseNegative = 0;
    };
}

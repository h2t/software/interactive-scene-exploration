#include "Statistics.h"

namespace interactive_scene_exploration
{

    void BrierScore::add(double pred, double gt)
    {
        double error = (pred - gt);
        sum += error * error;
        count += 1;
    }

    double BrierScore::get() const
    {
        return sum / count;
    }

    void Accuracy::add(bool pred, bool gt)
    {
        if (gt)
        {
            if (pred)
            {
                truePositive += 1;
            }
            else
            {
                falseNegative += 1;
            }
        }
        else
        {
            if (pred)
            {
                falsePositive += 1;
            }
            else
            {
                trueNegative += 1;
            }
        }
    }

    double Accuracy::precision() const
    {
        return (double)truePositive / (truePositive + falsePositive);
    }

    double Accuracy::recall() const
    {
        return (double)truePositive / (truePositive + falseNegative);
    }

    double Accuracy::accuracy() const
    {
        int trues = truePositive + trueNegative;
        int falses = falseNegative + falseNegative;
        return (double)trues / (trues + falses);
    }

    double Accuracy::F1() const
    {
        double precision = this->precision();
        double recall = this->recall();

        double F1 = 2.0 * (precision * recall) / (precision + recall);
        return F1;
    }


}

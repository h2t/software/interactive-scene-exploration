#pragma once

#include "RandomEngine.h"

#include <Eigen/Core>
#include <Eigen/Eigenvalues>

namespace interactive_scene_exploration
{

    template <int STATE_SIZE>
    struct Gaussian
    {
        using State = Eigen::Matrix<double, STATE_SIZE, 1>;
        using Covariance = Eigen::Matrix<double, STATE_SIZE, STATE_SIZE>;

        State mean;
        Covariance covariance;

        State sampleOne(RandomEngine& engine) const;

        std::vector<State> sampleMany(int numberOfSamples, RandomEngine& engine) const;
    };

    namespace detail
    {
        template <int STATE_SIZE>
        Eigen::Matrix<std::complex<double>, STATE_SIZE, STATE_SIZE> calculateScaledEigenVectors(
            typename Gaussian<STATE_SIZE>::Covariance const& covariance)
        {
            Eigen::EigenSolver<typename Gaussian<STATE_SIZE>::Covariance> eigensolver(covariance);

            auto eigVecs = eigensolver.eigenvectors();
            auto eigVals = eigensolver.eigenvalues();
            eigVals = eigVals.cwiseAbs();

            // Scale eigenvectors with eigenvalues:
            // D.Sqrt(); Z = Z * D; (for each column)
            eigVals = eigVals.array().sqrt();
            for (int i = 0; i < STATE_SIZE; i++)
            {
                eigVecs.col(i) *= eigVals[i];
            }

            return eigVecs;
        }

        template <int STATE_SIZE>
        auto sampleGaussianImplementation(RandomEngine& engine,
                                          Eigen::Matrix<double, STATE_SIZE, 1> const& mean,
                                          Eigen::Matrix<std::complex<double>, STATE_SIZE, STATE_SIZE> const& eigenVectors)
        {
            using G = Gaussian<STATE_SIZE>;

            typename G::State sample = G::State::Zero();

            std::normal_distribution<double> dist;
            for (int i = 0; i < STATE_SIZE; ++i)
            {
                double rnd = dist(engine);
                for (int d = 0; d < STATE_SIZE; ++d)
                {
                    sample[d] += eigenVectors.coeff(d, i).real() * rnd;
                }
            }
            for (int d = 0; d < STATE_SIZE; ++d)
            {
                sample[d] += mean[d];
            }
            return sample;
        }
    }



    template<int STATE_SIZE>
    auto Gaussian<STATE_SIZE>::sampleOne(RandomEngine& engine) const -> State
    {
        auto eigenVectors = detail::calculateScaledEigenVectors<STATE_SIZE>(covariance);

        return detail::sampleGaussianImplementation(engine, mean, eigenVectors);
    }

    template<int STATE_SIZE>
    auto Gaussian<STATE_SIZE>::sampleMany(int numberOfSamples, RandomEngine& engine) const -> std::vector<State>
    {
        auto eigenVectors = detail::calculateScaledEigenVectors<STATE_SIZE>(covariance);

        std::vector<State> result;
        result.resize(numberOfSamples);

        for (int i = 0; i < numberOfSamples; ++i)
        {
            result[i] = detail::sampleGaussianImplementation(engine, mean, eigenVectors);
        }

        return result;
    }

}

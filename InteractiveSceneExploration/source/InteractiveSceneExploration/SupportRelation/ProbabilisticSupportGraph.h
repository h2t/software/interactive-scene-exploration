#pragma once

#include <InteractiveSceneExploration/API.h>
#include <InteractiveSceneExploration/ObjectHypothesis/ObjectHypothesis.h>

#include <SemanticObjectRelations/RelationGraph/RelationGraph.h>
#include <SemanticObjectRelations/Serialization/json.h>


namespace interactive_scene_exploration
{

    struct ProbabilisticSupportVertex  : public semrel::ShapeVertex
    {
        ObjectHypothesis object;
    };

    struct ProbabilisticSupportEdge
    {
        double geometricExistenceProbability = 0.0;
        double topDownExistenceProbability = 0.0;
        bool topDownSupport = false;
        double meanSupportAreaOfTopDown = 0.0;
    };

    ISE_API void to_json(nlohmann::json& j, const ProbabilisticSupportVertex& vertex);
    ISE_API void from_json(const nlohmann::json& j, ProbabilisticSupportVertex& vertex);

    ISE_API void to_json(nlohmann::json& j, const ProbabilisticSupportEdge& edge);
    ISE_API void from_json(const nlohmann::json& j, ProbabilisticSupportEdge& edge);

    ISE_API void to_json(nlohmann::json& j, const ObjectHypothesis& object);
    ISE_API void from_json(const nlohmann::json& j, ObjectHypothesis& object);

    ISE_API void to_json(nlohmann::json& j, const BoxHypothesis& object);
    ISE_API void from_json(const nlohmann::json& j, BoxHypothesis& object);

    ISE_API void to_json(nlohmann::json& j, const CylinderHypothesis& object);
    ISE_API void from_json(const nlohmann::json& j, CylinderHypothesis& object);

    ISE_API void to_json(nlohmann::json& j, const SphereHypothesis& object);
    ISE_API void from_json(const nlohmann::json& j, SphereHypothesis& object);


    /**
     * @brief Representation of a binary support relation between scene objects.
     */
    class ISE_API ProbabilisticSupportGraph : public semrel::RelationGraph<ProbabilisticSupportVertex, ProbabilisticSupportEdge>
    {

    public:

        // Inherit contructors.
        using semrel::RelationGraph<ProbabilisticSupportVertex, ProbabilisticSupportEdge>::RelationGraph;

        std::string strVertex(ConstVertex v) const override;
        std::string strEdge(ConstEdge e) const override;

    };




}

#include "ProbabilisticSupportAnalysis.h"

#include <InteractiveSceneExploration/ObjectHypothesis/SceneDistribution.h>

#include <SemanticObjectRelations/SupportAnalysis/SupportAnalysis.h>
#include <SemanticObjectRelations/Hooks/Log.h>

namespace interactive_scene_exploration
{

    struct SupportEdgeStatAccu
    {
        int actCount = 0;

        int topDownCount = 0;
        std::vector<double> supportAreas;
    };

    ProbabilisticSupportAnalysisResult ProbabilisticSupportAnalysis::analyze(
        SceneDistribution const& scene,
        ProbabilisticSupportAnalysisParams const& params,
        RandomEngine& randomEngine)
    {
        ProbabilisticSupportAnalysisResult result;

        semrel::SupportAnalysis supportAnalysis;
        supportAnalysis.setGravityVector(params.support.gravityVector);
        supportAnalysis.setContactMargin(params.support.contactMargin);

        supportAnalysis.setVertSepPlaneAngleMax(params.support.vertSepPlaneAngleMax);
        supportAnalysis.setVertSepPlaneAssumeSupport(params.support.vertSepPlaneAssumeSupport);

        supportAnalysis.setUncertaintyDetectionEnabled(params.support.uncertaintyDetectionEnabled);
        supportAnalysis.setSupportAreaRatioMin(params.support.supportAreaRatioMin);

        std::map<std::pair<int, int>, SupportEdgeStatAccu> supportStatAccus;

        std::set<semrel::ShapeID> safeIDs;
        for (int groundLabel : params.support.groundLabels)
        {
            safeIDs.emplace(semrel::ShapeID{groundLabel});
        }

        {
            // Deterministic support extraction for comparison
            result.deterministicShapes = scene.getMean();
            semrel::ShapeMap shapeMap = semrel::toShapeMap(result.deterministicShapes);

            result.deterministicSupportGraph = supportAnalysis.performSupportAnalysis(shapeMap, safeIDs);
        }

        int numberOfSamples = params.numberOfSamples;
        for (int i = 0; i < numberOfSamples; ++i)
        {
            semrel::ShapeList& sampledScene = result.sceneSamples.emplace_back(
                                                  scene.sample(randomEngine));
            semrel::ShapeMap shapeMap = semrel::toShapeMap(sampledScene);

            semrel::SupportGraph& supportGraph = result.supportGraphs.emplace_back(
                    supportAnalysis.performSupportAnalysis(shapeMap, safeIDs));
            for (auto edge : supportGraph.edges())
            {
                semrel::ShapeID sourceID = edge.sourceObjectID();
                semrel::ShapeID targetID = edge.targetObjectID();
                std::pair<int, int> key(sourceID.t, targetID.t);
                SupportEdgeStatAccu& statAccu = supportStatAccus[key];

                if (edge.attrib().fromUncertaintyDetection)
                {
                    // Top-down support
                    statAccu.topDownCount += 1;

                    semrel::SupportVertex const& sourceAttrib = supportGraph.vertex(edge.sourceObjectID()).attrib();
                    statAccu.supportAreas.push_back(sourceAttrib.ud.supportAreaRatio);
                }
                else
                {
                    // Act support
                    statAccu.actCount += 1;
                }
            }
        }

        // Create an attributed graph
        ProbabilisticSupportGraph& graph = result.graph;

        for (auto& object : scene.objects)
        {
            ProbabilisticSupportVertex vertex;
            vertex.object = object;
            graph.addVertex(semrel::ShapeID{object.id}, vertex);
        }
        for (auto& pair : supportStatAccus)
        {
            int sourceID = pair.first.first;
            int targetID = pair.first.second;
            SupportEdgeStatAccu const& statAccu = pair.second;

            double actProbability = 1.0 * statAccu.actCount / numberOfSamples;

            SR_LOG_INFO << "ACT: [" << sourceID << "] -> [" << targetID << "]: " << actProbability;

            double tdProbability = 1.0 * statAccu.topDownCount / numberOfSamples;

            double meanSupportArea = 0.0;
            bool topDownSupport = statAccu.topDownCount > 0;
            if (topDownSupport)
            {
                double sum = 0.0;
                for (auto& sa : statAccu.supportAreas)
                {
                    sum += sa;
                }
                meanSupportArea = 1.0 * sum / statAccu.supportAreas.size();
                SR_LOG_INFO << "T-D: [" << sourceID << "] -> [" << targetID << "]: " << meanSupportArea;
            }

            auto sourceDesc = graph.vertex(semrel::ShapeID{sourceID});
            auto targetDesc = graph.vertex(semrel::ShapeID{targetID});

            ProbabilisticSupportEdge edge;
            edge.topDownSupport = topDownSupport;
            edge.geometricExistenceProbability = actProbability;
            edge.topDownExistenceProbability = tdProbability;
            edge.meanSupportAreaOfTopDown = meanSupportArea;

            graph.addEdge(sourceDesc, targetDesc, edge);
        }

        return result;
    }


}

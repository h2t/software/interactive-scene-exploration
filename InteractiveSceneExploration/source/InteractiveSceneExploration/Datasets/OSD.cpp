#include "OSD.h"

#include <InteractiveSceneExploration/PointCloud/IO.h>

#include <SimoxUtility/math/convert/rpy_to_mat3f.h>

#include <filesystem>
#include <algorithm>

namespace interactive_scene_exploration::osd
{

    std::vector<File> findFilesInDirectory(std::string const& directory, std::string const& extension)
    {
        std::vector<File> result;

        std::vector<std::filesystem::path> pcdFiles;

        for (auto& p : std::filesystem::directory_iterator(directory))
        {
            if (p.path().extension() == extension)
            {
                pcdFiles.push_back(p.path());

                File file;
                file.fullPath = p.path().string();

                std::string filenameWithoutExtenstion = p.path().filename().stem().string();

                int digitStart = -1;
                for (int i = 0; i < (int)filenameWithoutExtenstion.size(); ++i)
                {
                    if (std::isdigit(filenameWithoutExtenstion[i]))
                    {
                        digitStart = i;
                        break;
                    }
                }

                if (digitStart  < 0)
                {
                    continue;
                }

                std::string prefix = filenameWithoutExtenstion.substr(0, digitStart);
                std::string numberString = filenameWithoutExtenstion.substr(digitStart);

                file.prefix = prefix;
                file.number = std::stoi(numberString);

                result.push_back(file);
            }
        }

        std::sort(result.begin(), result.end(), [](File const & lhs, File const & rhs)
        {
            return std::tie(lhs.prefix, lhs.number) < std::tie(rhs.prefix, rhs.number);
        });

        return result;
    }

    LoadedDataset loadDataset(std::string const& directory)
    {
        LoadedDataset result;

        std::vector<osd::File> pcdFiles = findFilesInDirectory(directory + "/pcd", ".pcd");
        std::vector<osd::File> supportFiles = findFilesInDirectory(directory + "/support", ".csv");

        int size = (int)pcdFiles.size();
        int supportSize = (int)supportFiles.size();
        for (int i = 0; i < size; ++i)
        {
            auto& file = pcdFiles[i];

            LoadedPointCloud pc;
            if (file.prefix == "learn")
            {
                pc.prefix = Prefix::Learn;
            }
            else if (file.prefix == "test")
            {
                pc.prefix = Prefix::Test;
            }
            else
            {
                pc.prefix = Prefix::Unknown;
            }
            pc.number = file.number;

            if (!interactive_scene_exploration::PointCloudIO::loadFromPCD(file.fullPath, &pc.pointCloud))
            {
                std::cout << "Could not load PCD file '" << file.fullPath << "'." << std::endl;
                continue;
            }

            if (pc.prefix == Prefix::Test)
            {
                pc.rotation.roll = -2.4f;
                if (pc.number == 12)
                {
                    pc.rotation.roll = -2.26f;
                }
                if (pc.number == 24)
                {
                    pc.rotation.roll = -2.30f;
                }
                if (pc.number >= 27 && pc.number <= 30)
                {
                    pc.rotation.roll = -2.30f;
                }
                if (pc.number >= 31 && pc.number <= 48)
                {
                    pc.rotation.roll = -2.27f;
                }
                if (pc.number >= 49 && pc.number <= 65)
                {
                    pc.rotation.roll = -2.32f;
                }
            }

            Eigen::Matrix3f rotation = simox::math::rpy_to_mat3f(pc.rotation.roll,
                                       pc.rotation.pitch,
                                       pc.rotation.yaw);

            auto& points = pc.pointCloud.points;
            int pointsSize = (int)points.size();
            for (int i = 0; i < pointsSize; ++i)
            {
                Eigen::Vector3f global = rotation * points[i].getVector3fMap();
                if (pc.crop.minX <= global.x() && global.x() <= pc.crop.maxX &&
                    pc.crop.minY <= global.y() && global.y() <= pc.crop.maxY)
                {
                    auto& filteredP = pc.filteredCloud.points.emplace_back(points[i]);
                    filteredP.getVector3fMap() = 1000.0f * global;
                    filteredP.label -= 1;

                    if (pc.number == 46)
                    {
                        // Relabel Segment 6 (it contains two objects!)
                        if (filteredP.label == 6 && global.x() > 0.075f)
                        {
                            filteredP.label = 8;
                        }
                    }
                }
            }

            pc.filteredCloud.height = 1;
            pc.filteredCloud.width = pc.filteredCloud.points.size();

            if (i < supportSize)
            {
                // We have a ground-truth file to define support relations
                auto& supportFile = supportFiles[i];
                if (supportFile.prefix != file.prefix)
                {
                    std::cerr << "Prefix of files is different: \n"
                              << "PCD: " << file.fullPath << "\n"
                              << "Sup: " << supportFile.fullPath << "\n";
                    continue;
                }
                if (supportFile.number != file.number)
                {
                    std::cerr << "Number of files is different: \n"
                              << "PCD: " << file.fullPath << "\n"
                              << "Sup: " << supportFile.fullPath << "\n";
                    continue;
                }

                pc.groundTruthSupportRelations = readSupportRelationsFromCSV(supportFiles[i].fullPath);
            }

            result.pointClouds.push_back(std::move(pc));
        }

        return result;
    }

    const char* prefixToString(Prefix prefix)
    {
        static const char* NAMES[] =
        {
            "Unknown",
            "Learn",
            "Test",
        };
        static std::size_t NAMES_SIZE = sizeof(NAMES) / sizeof(NAMES[0]);

        std::size_t prefixIndex = (std::size_t)prefix;
        if (prefixIndex < NAMES_SIZE)
        {
            return NAMES[prefixIndex];
        }
        else
        {
            return "(Unexpected Value)";
        }
    }

}

#pragma once

#include "Dataset.h"

#include <InteractiveSceneExploration/API.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <string>
#include <vector>

namespace interactive_scene_exploration::osd
{
    struct File
    {
        std::string fullPath;

        std::string prefix;
        int number = -1;
    };

    enum class Prefix
    {
        Unknown,
        Learn,
        Test,
    };

    using Point = pcl::PointXYZRGBL;
    using PointCloud = pcl::PointCloud<Point>;

    struct PointCloudRotation
    {
        float roll = -2.30f;
        float pitch = 0.0f;
        float yaw = 0.0f;
    };

    struct PointCloudCrop
    {
        float minX = -0.5f;
        float maxX = 0.3f;
        float minY = 0.0f;
        float maxY = 1.2f;
    };

    struct LoadedPointCloud
    {
        Prefix prefix = Prefix::Unknown;
        int number = -1;

        PointCloudRotation rotation;
        PointCloudCrop crop;

        PointCloud pointCloud;
        PointCloud filteredCloud;

        std::vector<SupportRelation> groundTruthSupportRelations;
    };

    struct LoadedDataset
    {
        std::vector<LoadedPointCloud> pointClouds;
    };

    ISE_API const char* prefixToString(Prefix prefix);

    ISE_API LoadedDataset loadDataset(std::string const& directory);
}

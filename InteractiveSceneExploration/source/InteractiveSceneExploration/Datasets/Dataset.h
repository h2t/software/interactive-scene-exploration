#pragma once

#include <InteractiveSceneExploration/API.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <string>
#include <vector>

namespace interactive_scene_exploration
{
    using Point = pcl::PointXYZRGBL;
    using PointCloud = pcl::PointCloud<Point>;

    struct SupportRelation
    {
        int source = 0;
        int target = 0;
    };

    struct DatasetEntry
    {
        std::string id;

        PointCloud pointCloud;

        std::vector<SupportRelation> supportRelations;
    };

    struct Dataset
    {
        std::string id;
        std::string name;

        std::vector<DatasetEntry> entries;
    };

    ISE_API std::vector<SupportRelation> readSupportRelationsFromCSV(std::string const& filePath);

}

#include "KIT-SRD.h"

#include <InteractiveSceneExploration/PointCloud/IO.h>

#include <filesystem>

namespace interactive_scene_exploration::kit_srd
{

    std::vector<std::filesystem::path> findSubDirectories(std::string const& directory)
    {
        std::vector<std::filesystem::path> result;

        for (auto& p : std::filesystem::directory_iterator(directory))
        {
            if (p.is_directory())
            {
                result.push_back(p.path());
            }
        }

        return result;
    }

    Dataset findDataset(std::string const& directory)
    {
        Dataset result;
        result.directory = directory;

        std::vector<std::filesystem::path> subDirectories = findSubDirectories(directory);
        for (auto& subDir : subDirectories)
        {
            std::string subDirName = subDir.filename();
            int number = std::stoi(subDirName);

            auto imageRgb = subDir / "rgb.bmp";
            if (!std::filesystem::is_regular_file(imageRgb))
            {
                std::cout << "rgb.bmp missing in " << subDir << std::endl;
                continue;
            }
            auto imageDepth = subDir / "depth.bmp";
            if (!std::filesystem::is_regular_file(imageDepth))
            {
                std::cout << "depth.bmp missing in " << subDir << std::endl;
                continue;
            }
            auto pcOriginal = subDir / "original.pcd";
            if (!std::filesystem::is_regular_file(pcOriginal))
            {
                std::cout << "original.pcd missing in " << subDir << std::endl;
                continue;
            }
            auto pcGlobal = subDir / "global.pcd";
            if (!std::filesystem::is_regular_file(pcGlobal))
            {
                std::cout << "global.pcd missing in " << subDir << std::endl;
                continue;
            }
            auto pcLabeled = subDir / "labeled.pcd";
            if (!std::filesystem::is_regular_file(pcLabeled))
            {
                std::cout << "labeled.pcd missing in " << subDir << std::endl;
                continue;
            }
            auto supportRelations = subDir / "support.csv";
            if (!std::filesystem::is_regular_file(supportRelations))
            {
                std::cout << "support.csv missing in " << subDir << std::endl;
                continue;
            }

            Entry entry;
            entry.number = number;
            entry.imageRgb = imageRgb.string();
            entry.imageDepth = imageDepth.string();
            entry.pcOriginal = pcOriginal.string();
            entry.pcGlobal = pcGlobal.string();
            entry.pcLabeled = pcLabeled.string();
            entry.supportRelations = supportRelations.string();

            result.entries.push_back(entry);
        }

        std::sort(result.entries.begin(), result.entries.end(), [](Entry const & left, Entry const & right)
        {
            return left.number < right.number;
        });

        return result;
    }

    //    static void convertPC(std::string const& filename)
    //    {
    //        PointCloud pc;
    //        if (interactive_scene_exploration::PointCloudIO::loadFromPCD(filename, &pc))
    //        {
    //            std::cout << "Working on: " << filename << std::endl;
    //            std::remove(filename.c_str());
    //            interactive_scene_exploration::PointCloudIO::saveToPCD(filename, pc);
    //        }
    //        else
    //        {
    //            std::cerr << "Could not load point cloud: " << filename << "\n";
    //        }
    //    }

    LoadedDataset loadDataset(const std::string& directory)
    {
        LoadedDataset result;
        result.directory = directory;

        Dataset dataset = findDataset(directory);
        for (auto& entry : dataset.entries)
        {
            LoadedEntry loadedEntry;
            loadedEntry.entry = entry;
            loadedEntry.number = entry.number;

            //            // Convert Point cloud format
            //            convertPC(entry.pcOriginal);
            //            convertPC(entry.pcGlobal);
            //            convertPC(entry.pcLabeled);


            if (!interactive_scene_exploration::PointCloudIO::loadFromPCD(entry.pcLabeled, &loadedEntry.labeledPointCloud))
            {
                std::cerr << "Could not load point cloud from: " << entry.pcLabeled << "\n";
                continue;
            }
            if (!interactive_scene_exploration::PointCloudIO::loadFromPCD(entry.pcGlobal, &loadedEntry.globalPointCloud))
            {
                std::cerr << "Could not load point cloud from: " << entry.pcGlobal << "\n";
                continue;
            }
            loadedEntry.supportRelations = readSupportRelationsFromCSV(entry.supportRelations);

            result.entries.push_back(loadedEntry);
        }

        return result;
    }

}

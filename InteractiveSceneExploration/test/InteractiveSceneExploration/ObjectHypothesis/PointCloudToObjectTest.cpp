
#include "../MatrixChecks.h"

#include <InteractiveSceneExploration/ObjectHypothesis/PointCloudToScene.h>

#include <SemanticObjectRelations/ShapeExtraction/PointCloudSegments.h>
#include <SimoxUtility/math/convert/mat3f_to_rpy.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>


#include <Eigen/Dense>

#include <future>



using namespace interactive_scene_exploration;


BOOST_AUTO_TEST_SUITE(PointCloudToObjectTest)


BOOST_AUTO_TEST_CASE(SimplePointCloud)
{
    // load point cloud
    std::string pcdFilename = "/home/paus/repos/semantic-object-relations/evaluation/pointclouds/R3.pcd";
    std::cout << "Loading point cloud from '" << pcdFilename << "' ..." << std::endl;
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr pointcloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
    if (pcl::io::loadPCDFile<pcl::PointXYZRGBL>(pcdFilename, *pointcloud))
    {
        std::cout << "Could not load PCD file '" << pcdFilename << "'." << std::endl;;
        BOOST_REQUIRE(false);
    }
    std::cout << "Loaded point cloud with " << pointcloud->size() << " points." << std::endl;

    semrel::PointCloudSegments segments(*pointcloud);
    std::vector<uint32_t> labels = segments.getLabels();

    std::vector<ObjectHypothesis> objects;

    std::cout << "Objects: " << objects.size() << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()


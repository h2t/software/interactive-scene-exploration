#include "../MatrixChecks.h"

#include <InteractiveSceneExploration/Math/PoseGaussian.h>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include <Eigen/Geometry>


using namespace interactive_scene_exploration;




BOOST_AUTO_TEST_SUITE(PoseGaussianTest)


BOOST_AUTO_TEST_CASE(FixedDisplacement)
{
    Matrix6d cov = Matrix6d::Identity();

    {
        PoseGaussian a{Displacement(Eigen::Vector3d(1.0, 2.0, 3.0),
                                    Eigen::Matrix3d::Identity()),
                      cov};

        Displacement disp(Eigen::Vector3d(10.0, 20.0, 30.0),
                          Eigen::Matrix3d::Identity());

        PoseGaussian r = disp * a;
        CHECK_VECTOR_CLOSE(r.mean.trans, Eigen::Vector3d(11.0, 22.0, 33.0), 1.0e-5);
        CHECK_QUAT_CLOSE(r.mean.rot, Eigen::Quaterniond::Identity(), 1.0e-5);

        // What happens with the covariance?
    }
}

BOOST_AUTO_TEST_CASE(UncertainDisplacement)
{
    BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(FusionTranslation)
{
    Matrix6d cov = Matrix6d::Identity();

    {
        PoseGaussian a{Displacement(Eigen::Vector3d(1.0, 2.0, 3.0),
                                    Eigen::Matrix3d::Identity()),
                      cov};

        PoseGaussian b{Displacement(Eigen::Vector3d(3.0, 6.0, 9.0),
                                    Eigen::Matrix3d::Identity()),
                      cov};

        PoseGaussian r = fuse(a, b);
        CHECK_VECTOR_CLOSE(r.mean.trans, Eigen::Vector3d(2.0, 4.0, 6.0), 1.0e-5);
        CHECK_QUAT_CLOSE(r.mean.rot, Eigen::Quaterniond::Identity(), 1.0e-5);
    }

    {
        PoseGaussian a{Displacement(Eigen::Vector3d(1.0, 2.0, 3.0),
                                    Eigen::Matrix3d::Identity()),
                      0.5 * cov};

        PoseGaussian b{Displacement(Eigen::Vector3d(4.0, 8.0, 12.0),
                                    Eigen::Matrix3d::Identity()),
                      cov};

        PoseGaussian r = fuse(a, b);
        CHECK_VECTOR_CLOSE(r.mean.trans, Eigen::Vector3d(2.0, 4.0, 6.0), 1.0e-5);
        CHECK_QUAT_CLOSE(r.mean.rot, Eigen::Quaterniond::Identity(), 1.0e-5);
    }

}

BOOST_AUTO_TEST_CASE(FusionRotation)
{
    Matrix6d cov = Matrix6d::Identity();
    Eigen::Vector3d pos = Eigen::Vector3d::Zero();

    {
        PoseGaussian a{Displacement(pos,
                                    Eigen::Matrix3d::Identity()
                                    ),
                      cov};

        PoseGaussian b{Displacement(pos,
                                    Eigen::AngleAxisd(M_PI, Eigen::Vector3d::UnitZ()).toRotationMatrix()
                                    ),
                      cov};

        PoseGaussian r = fuse(a, b);
        CHECK_VECTOR_CLOSE(r.mean.trans, pos, 1.0e-5);
        Eigen::Matrix3d expectedRot = Eigen::AngleAxisd(M_PI / 2.0, Eigen::Vector3d::UnitZ()).toRotationMatrix();
        CHECK_QUAT_CLOSE(r.mean.rot, Eigen::Quaterniond(expectedRot), 1.0e-5);
    }

    {
        PoseGaussian a{Displacement(pos,
                                    Eigen::Matrix3d::Identity()
                                    ),
                    0.5 * cov};

        PoseGaussian b{Displacement(pos,
                                    Eigen::AngleAxisd(M_PI, Eigen::Vector3d::UnitZ()).toRotationMatrix()
                                    ),
                    cov};

        PoseGaussian r = fuse(a, b);
        CHECK_VECTOR_CLOSE(r.mean.trans, pos, 1.0e-5);
        // We interpolate based on the covariance between 0 rad and \pi rad
        // Since 0 rad has half the covariance, the result is ((2 * 0) + (1 * \pi)) / 3
        Eigen::Matrix3d expectedRot = Eigen::AngleAxisd(M_PI / 3.0, Eigen::Vector3d::UnitZ()).toRotationMatrix();
        CHECK_QUAT_CLOSE(r.mean.rot, Eigen::Quaterniond(expectedRot), 1.0e-5);
    }

}

BOOST_AUTO_TEST_CASE(FusionFull)
{
    // TODO: How does the fusion work for combined translation and rotation?
    // We need test cases here
    BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END()


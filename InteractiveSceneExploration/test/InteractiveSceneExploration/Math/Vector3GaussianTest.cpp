
#include "../MatrixChecks.h"

#include <mrpt/poses/CPointPDFGaussian.h>
#include <mrpt/poses/CPose3D.h>

#include <InteractiveSceneExploration/Math/Vector3Gaussian.h>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>


#include <Eigen/Dense>



using namespace interactive_scene_exploration;


BOOST_AUTO_TEST_SUITE(Vector3GaussianTest)


BOOST_AUTO_TEST_CASE(Transform)
{
    Eigen::Vector3d pos(1.0, 2.0, 3.0);
    Eigen::Matrix3d cov = Eigen::Matrix3d::Identity();

    Vector3Gaussian v{pos, cov};

    {
        // Zero transform
        Vector3Gaussian r = transform(Eigen::Matrix3d::Identity(), Eigen::Vector3d::Zero(), v);
        CHECK_VECTOR_CLOSE(r.mean, v.mean, 1.0e-5);
        CHECK_MATRIX_CLOSE(r.covariance, v.covariance, 1.0e-5);
    }
    {
        // Translation
        Eigen::Vector3d transl = Eigen::Vector3d(10.0, 20.0, 30.0);
        Vector3Gaussian r = transform(Eigen::Matrix3d::Identity(), transl, v);

        Eigen::Vector3d expectedPos = pos + transl;
        CHECK_VECTOR_CLOSE(r.mean, expectedPos, 1.0e-5);
        CHECK_MATRIX_CLOSE(r.covariance, v.covariance, 1.0e-5);
    }
    {
        // Rotation
        Eigen::Vector3d transl = Eigen::Vector3d::Zero();
        Eigen::Matrix3d rot = Eigen::AngleAxisd(M_PI/2, Eigen::Vector3d::UnitY()).toRotationMatrix();
        Vector3Gaussian r = transform(rot, transl, v);

        // Result of rotation
        Eigen::Vector3d expectedPos(3.0, 2.0, -1.0);
        CHECK_VECTOR_CLOSE(r.mean, expectedPos, 1.0e-5);
        // The covariance should not change due to a rotation
        CHECK_MATRIX_CLOSE(r.covariance, v.covariance, 1.0e-5);
    }
    {
        // Translation and rotation
        Eigen::Vector3d transl(10.0, 20.0, 30.0);
        Eigen::Matrix3d rot = Eigen::AngleAxisd(M_PI/2, Eigen::Vector3d::UnitY()).toRotationMatrix();
        Vector3Gaussian r = transform(rot, transl, v);

        // Result of rotation
        Eigen::Vector3d rotatedPos(3.0, 2.0, -1.0);
        Eigen::Vector3d expectedPos = rotatedPos + transl;
        CHECK_VECTOR_CLOSE(r.mean, expectedPos, 1.0e-5);

        // The covariance should not change due to a rotation
        CHECK_MATRIX_CLOSE(r.covariance, v.covariance, 1.0e-5);
    }
}

static mrpt::poses::CPoint3D toMRPT(Eigen::Vector3d p)
{
    mrpt::poses::CPoint3D r(p.x(), p.y(), p.z());
    return r;
}

static mrpt::math::CMatrixDouble33 toMRPT(Eigen::Matrix3d i)
{
    mrpt::math::CMatrixDouble33 r = i;
    return r;
}

BOOST_AUTO_TEST_CASE(MRPTTransform)
{
    Eigen::Vector3d pos(1.0, 2.0, 3.0);
    Eigen::Matrix3d cov = Eigen::Matrix3d::Identity();
    mrpt::poses::CPointPDFGaussian v(toMRPT(pos), toMRPT(cov));
    {
        // Zero transform
        mrpt::poses::CPose3D transform(Eigen::Matrix3d::Identity(), Eigen::Vector3d::Zero());
        mrpt::poses::CPointPDFGaussian r = v;
        r.changeCoordinatesReference(transform);

        CHECK_VECTOR_CLOSE(r.mean.m_coords, v.mean.m_coords, 1.0e-5);
        CHECK_MATRIX_CLOSE(r.cov, v.cov, 1.0e-5);
    }
    {
        // Translation
        Eigen::Vector3d transl = Eigen::Vector3d(10.0, 20.0, 30.0);
        mrpt::poses::CPose3D transform(Eigen::Matrix3d::Identity(), transl);
        mrpt::poses::CPointPDFGaussian r = v;
        r.changeCoordinatesReference(transform);

        Eigen::Vector3d expectedPos = pos + transl;
        CHECK_VECTOR_CLOSE(r.mean.m_coords, expectedPos, 1.0e-5);
        CHECK_MATRIX_CLOSE(r.cov, v.cov, 1.0e-5);
    }
    {
        // Rotation
        Eigen::Vector3d transl = Eigen::Vector3d::Zero();
        Eigen::Matrix3d rot = Eigen::AngleAxisd(M_PI/2, Eigen::Vector3d::UnitY()).toRotationMatrix();
        mrpt::poses::CPose3D transform(rot, transl);
        mrpt::poses::CPointPDFGaussian r = v;
        r.changeCoordinatesReference(transform);

        // Result of rotation
        Eigen::Vector3d expectedPos(3.0, 2.0, -1.0);
        CHECK_VECTOR_CLOSE(r.mean.m_coords, expectedPos, 1.0e-5);
        // The covariance should not change due to a rotation
        CHECK_MATRIX_CLOSE(r.cov, v.cov, 1.0e-5);
    }
    {
        // Translation and rotation
        Eigen::Vector3d transl(10.0, 20.0, 30.0);
        Eigen::Matrix3d rot = Eigen::AngleAxisd(M_PI/2, Eigen::Vector3d::UnitY()).toRotationMatrix();
        mrpt::poses::CPose3D transform(rot, transl);
        mrpt::poses::CPointPDFGaussian r = v;
        r.changeCoordinatesReference(transform);

        // Result of rotation
        Eigen::Vector3d rotatedPos(3.0, 2.0, -1.0);
        Eigen::Vector3d expectedPos = rotatedPos + transl;
        CHECK_VECTOR_CLOSE(r.mean.m_coords, expectedPos, 1.0e-5);

        // The covariance should not change due to a rotation
        CHECK_MATRIX_CLOSE(r.cov, v.cov, 1.0e-5);
    }
}

BOOST_AUTO_TEST_CASE(MRPTFusion)
{
    Eigen::Matrix3d cov = Eigen::Matrix3d::Identity();

    {
        mrpt::poses::CPointPDFGaussian a(toMRPT(Eigen::Vector3d(1.0, 2.0, 3.0)), toMRPT(cov));
        mrpt::poses::CPointPDFGaussian b(toMRPT(Eigen::Vector3d(3.0, 6.0, 9.0)), toMRPT(cov));

        // Same covariance should result in averaging
        mrpt::poses::CPointPDFGaussian r;
        r.bayesianFusion(a, b);
        Eigen::Vector3d expectedMean(2.0, 4.0, 6.0);
        CHECK_VECTOR_CLOSE(r.mean.m_coords, expectedMean, 1.0e-5);
    }
    {
        mrpt::poses::CPointPDFGaussian a(toMRPT(Eigen::Vector3d(1.0, 2.0, 3.0)), toMRPT(cov));
        Eigen::Matrix3d cov2 = 2.0 * cov;
        mrpt::poses::CPointPDFGaussian b(toMRPT(Eigen::Vector3d(4.0, 8.0, 12.0)), toMRPT(cov2));

        // Covariance works as a weight
        // b's covariance is twice as high, meaning it has half the influence as a
        mrpt::poses::CPointPDFGaussian r;
        r.bayesianFusion(a, b);
        Eigen::Vector3d expectedMean(2.0, 4.0, 6.0);
        CHECK_VECTOR_CLOSE(r.mean.m_coords, expectedMean, 1.0e-5);
        Eigen::Matrix3d expectedCov = 2.0/3.0 * Eigen::Matrix3d::Identity();
        CHECK_MATRIX_CLOSE(r.cov, expectedCov, 1.0e-5);
    }
    // What exactly happens with non-diagonal covariances?
    // TODO: We need a test for that
}

BOOST_AUTO_TEST_CASE(Fusion)
{
    Eigen::Matrix3d cov = Eigen::Matrix3d::Identity();

    {
        Vector3Gaussian a{Eigen::Vector3d(1.0, 2.0, 3.0), cov};
        Vector3Gaussian b{Eigen::Vector3d(3.0, 6.0, 9.0), cov};

        // Same covariance should result in averaging
        Vector3Gaussian r = fuse(a, b);
        Eigen::Vector3d expectedMean(2.0, 4.0, 6.0);
        CHECK_VECTOR_CLOSE(r.mean, expectedMean, 1.0e-5);
    }
    {
        Vector3Gaussian a{Eigen::Vector3d(1.0, 2.0, 3.0), cov};
        Vector3Gaussian b{Eigen::Vector3d(4.0, 8.0, 12.0), 2.0*cov};

        // Covariance works as a weight
        // b's covariance is twice as high, meaning it has half the influence as a
        Vector3Gaussian r = fuse(a, b);
        Eigen::Vector3d expectedMean(2.0, 4.0, 6.0);
        CHECK_VECTOR_CLOSE(r.mean, expectedMean, 1.0e-5);
        Eigen::Matrix3d expectedCov = 2.0/3.0 * Eigen::Matrix3d::Identity();
        CHECK_MATRIX_CLOSE(r.covariance, expectedCov, 1.0e-5);
    }
    // What exactly happens with non-diagonal covariances?
    // TODO: We need a test for that
}

BOOST_AUTO_TEST_SUITE_END()


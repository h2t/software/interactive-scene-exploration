
#include <InteractiveSceneExploration/Math/Twist.h>
#include <InteractiveSceneExploration/Math/Displacement.h>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

BOOST_AUTO_TEST_SUITE(TwistTest)

using namespace interactive_scene_exploration;

BOOST_AUTO_TEST_CASE(DisplacementConversionZero)
{
    Twist zero = Twist::Zero();
    Displacement disp = zero.exp();
    Eigen::Matrix4d transform = disp.toHomogeneousMatrix();

    // A zero twist should result in an identity transformation
    Eigen::Matrix4d diff = transform - Eigen::Matrix4d::Identity();

    for (int x = 0; x < 4; ++x)
    {
        for (int y = 0; y < 4; ++y)
        {
            BOOST_CHECK_SMALL(diff(x, y), 1.0e-5);
        }
    }
}

BOOST_AUTO_TEST_CASE(DisplacementConversionTransl)
{
    Twist twist(Eigen::Vector3d(1.0, 2.0, 3.0), Eigen::Vector3d::Zero());
    Displacement disp = twist.exp();
    Eigen::Matrix4d transform = disp.toHomogeneousMatrix();
    Eigen::Matrix4d diff = transform - Eigen::Matrix4d::Identity();

    for (int x = 0; x < 4; ++x)
    {
        for (int y = 0; y < 3; ++y)
        {
            BOOST_CHECK_SMALL(diff.col(y)(x), 1.0e-5);
        }
    }

    // A twist with only linear part should result in a translation
    Eigen::Vector4d col3 = diff.col(3);
    BOOST_CHECK_CLOSE(col3.x(), 1.0, 1e-05);
    BOOST_CHECK_CLOSE(col3.y(), 2.0, 1e-05);
    BOOST_CHECK_CLOSE(col3.z(), 3.0, 1e-05);
    BOOST_CHECK_CLOSE(col3.w(), 0.0, 1e-05);
}

BOOST_AUTO_TEST_CASE(DisplacementConversionRotation)
{
    double angle = M_PI/2.0;
    Eigen::Vector3d axis = Eigen::Vector3d(1.0, 0.0, 0.0);
    Eigen::Matrix3d rotationMatrix = Eigen::AngleAxisd(angle, axis).toRotationMatrix();
    Eigen::Matrix4d expectedTransform = Eigen::Matrix4d::Identity();
    expectedTransform.block<3, 3>(0, 0) = rotationMatrix;

    Twist twist(Eigen::Vector3d::Zero(), angle * axis);
    Displacement disp = twist.exp();
    Eigen::Matrix4d transform = disp.toHomogeneousMatrix();
    Eigen::Matrix4d diff = transform - expectedTransform;

    for (int x = 0; x < 4; ++x)
    {
        for (int y = 0; y < 4; ++y)
        {
            BOOST_CHECK_SMALL(diff(x, y), 1.0e-5);
        }
    }
}

BOOST_AUTO_TEST_CASE(DisplacementConversionFull)
{
    double angle = M_PI/2.0;
    Eigen::Vector3d axis = Eigen::Vector3d(0.0, 1.0, 0.0);
    Eigen::Matrix3d rotationMatrix = Eigen::AngleAxisd(angle, axis).toRotationMatrix();
    Eigen::Vector3d transl(5.0, 6.0, 7.0);
    Eigen::Matrix4d expectedTransform = Eigen::Matrix4d::Identity();
    expectedTransform.block<3, 3>(0, 0) = rotationMatrix;
    expectedTransform.block<3, 1>(0, 3) = transl;

    Twist twist(transl, angle * axis);
    Displacement disp = twist.exp();
    Eigen::Matrix4d transform = disp.toHomogeneousMatrix();
    Eigen::Matrix4d diff = transform - expectedTransform;

    // We only check the rotation part (the translation part changes)
    for (int x = 0; x < 3; ++x)
    {
        for (int y = 0; y < 3; ++y)
        {
            BOOST_CHECK_SMALL(diff(x, y), 1.0e-5);
        }
    }

    // Check the runtrip: log(exp(twist)) = twist
    Displacement newDispl(transform);
    Twist newTwist = newDispl.log();

    Vector6d twistDiff = newTwist.data - twist.data;
    for (int i = 0; i < 6; ++i)
    {
        BOOST_CHECK_SMALL(twistDiff(i), 1.0e-05);
    }
}

BOOST_AUTO_TEST_SUITE_END()


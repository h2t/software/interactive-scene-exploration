#pragma once

#define CHECK_VECTOR_CLOSE(a, b, tolerance) \
{ \
    Eigen::Vector3d diff = a - b; \
    for (int i = 0; i < diff.rows(); ++i) \
    { \
        BOOST_CHECK_SMALL(diff(i), tolerance); \
    } \
}

#define CHECK_MATRIX_CLOSE(a, b, tolerance) \
{ \
    Eigen::Matrix3d diff = a - b; \
    for (int col = 0; col < diff.cols(); ++col) \
    { \
        for (int row = 0; row < diff.rows(); ++row) \
        { \
            BOOST_CHECK_SMALL(diff.col(col)(row), tolerance); \
        } \
    } \
}

#define CHECK_QUAT_CLOSE(a, b, tolerance) \
{ \
    BOOST_CHECK_SMALL(a.w() - b.w(), tolerance); \
    BOOST_CHECK_SMALL(a.x() - b.x(), tolerance); \
    BOOST_CHECK_SMALL(a.y() - b.y(), tolerance); \
    BOOST_CHECK_SMALL(a.z() - b.z(), tolerance); \
}

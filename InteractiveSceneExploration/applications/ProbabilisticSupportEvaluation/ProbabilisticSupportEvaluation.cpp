#include <InteractiveSceneExploration/Evaluation/ProbabilisticSupportEvaluation.h>
#include <InteractiveSceneExploration/Datasets/DatasetLoader.h>

#include <fstream>
#include <cstdlib>
#include <cstdio>

using namespace interactive_scene_exploration;

int main(int argc, char* argv[])
{
    std::string argument1 = "OSD";
    if (argc > 1)
    {
        argument1 = argv[1];
    }
    std::string argument2 = "/home/paus/projects/2020/interactive-scene-exploration/evaluation/data/OSD";
    if (argc > 2)
    {
        argument2 = argv[2];
    }
    std::string argument3 = "evaluation.txt";
    if (argc > 3)
    {
        argument3 = argv[3];
    }
    if (argument1 == "--help" || argument1 == "--h")
    {
        std::puts("ProbabilisticSupportEvaluation [dataset-name] [dataset-directory] [evaluation-output-file]\n"
                  "  [dataset-name]: Name of the dataset (OSD or KIT-SR)\n"
                  "  [dataset-directory]: Directory which contains the OSD (Object Segmentation Dataset)\n"
                  "  [evaluation-output-file]: File into which the evaluation results will be output (default: evaluation.txt)\n");
        return 0;
    }

    std::string datasetName = argument1;
    std::string datasetDirectory = argument2;
    std::string evaluationOutputFile = argument3;

    DatasetName name;
    if (datasetName == "OSD")
    {
        name = DatasetName::OSD;
    }
    else if (datasetName == "KIT-SR")
    {
        name = DatasetName::KIT_SR;
    }
    else
    {
        std::printf("Unknown dataset name '%s'. Expected 'OSD' or 'KIT-SR'.\n",
                    datasetName.c_str());
        return -1;
    }

    Dataset dataset = DatasetLoader::load(name, datasetDirectory);
    if (dataset.entries.size() == 0)
    {
        std::printf("Could not read dataset from directory '%s'.\n"
                    "No files with the correct name pattern found.\n",
                    datasetDirectory.c_str());
        return -2;
    }

    ProbabilisticSupportEvaluationParams params;
    // In the datasets, the label 0 is always the table plane, i.e. stable ground
    params.support.support.groundLabels.insert(0);

    RandomEngine randomEngine;
    // Define a fixed seed to get reproducable results
    // Replace with:
    //   randomEngine.seed(std::random_engine());
    // to get slightly different results every time.
    randomEngine.seed(12345ul);
    ProbabilisticSupportEvaluationResult result = ProbabilisticSupportEvaluation::evaluateOnDataset(params, dataset, randomEngine);

    // Output results to file
    try
    {
        std::ofstream out;
        out.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        out.open(evaluationOutputFile);

        out << "Probabilistic support evaluation " << datasetName << "\n";
        out << "Deterministic: \n";
        out << "  Precision: " << result.deterministic.precision << "\n"
            << "  Recall: " << result.deterministic.recall << "\n"
            << "  Accuracy: " << result.deterministic.accuracy << "\n"
            << "  F1 score: " << result.deterministic.F1 << "\n"
            << "  Brier score: " << result.deterministic.brierScore << "\n"
            << "\n";
        out << "Probabilistic: \n";
        out << "  Precision: " << result.probabilistic.precision << "\n"
            << "  Recall: " << result.probabilistic.recall << "\n"
            << "  Accuracy: " << result.probabilistic.accuracy << "\n"
            << "  F1 score: " << result.probabilistic.F1 << "\n"
            << "  Brier score: " << result.probabilistic.brierScore << "\n"
            << "\n";

        out.close();
    }
    catch (std::exception const& ex)
    {
        std::printf("Error while trying to write to file '%s'.\n"
                    "Reason: %s",
                    evaluationOutputFile.c_str(), ex.what());
        return -3;
    }


    return 0;
}


/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::PickAndPlaceComponent
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/components/ObjectPoseObserver/plugins/ObjectPoseClientPlugin.h>
#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <ArmarXGui/libraries/RemoteGui/Client/EigenWidgets.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

#include <Eigen/Core>

#include <atomic>


namespace armarx
{
    using namespace RemoteGui::Client;
    struct PickAndPlaceTab : Tab
    {
        ComboBox selectSide;
        ComboBox selectAction;
        ComboBox selectObject;
        ComboBox selectGrasp;
        Vector3Widget placePosition;
        Vector3Widget supportPosition;
        Button saveObjectPoses;
        CheckBox autoUpdateGraspCandidates;
        CheckBox autoUpdatePlaceCandidate;
        Button calculateGraspCandidates;
        Button executeAction;
        Button loadPlan;
        ComboBox planSteps;
        Button executePlanStep;
        Button stop;
    };

    struct PlanStep
    {
        std::string objectName;
        Eigen::Vector3f placingPosition;
    };

    struct Plan
    {
        std::vector<PlanStep> steps;
    };

    enum Side
    {
        Side_Left,
        Side_Right,
    };

    enum Action
    {
        Action_Begin,

        Action_None = Action_Begin,
        Action_Retreat,
        Action_Prepose,
        Action_PreposeReverse,
        Action_Push,
        Action_Pick,
        Action_Place,
        Action_SupportPrepose,
        Action_SupportPreposeReverse,
        Action_Support,
        Action_SupportRetreat,

        Action_End
    };

    struct GeneralConfig
    {
        bool IsSimulation = false;
        float MaximalDistanceToStartInMM = 300.0f;
        float SimulationForceHeightThresholdInMM = 1050.0f;
        float ForceThresholdInPotatoes = 5.0f;
    };

    struct PreposeSingleConfig
    {
        std::vector<Eigen::Matrix4f> Waypoints;
    };

    struct PreposeConfig
    {
        PreposeSingleConfig Left;
        PreposeSingleConfig Right;
    };


    struct PickAndPlaceConfig
    {
        GeneralConfig General;
        PreposeConfig Prepose;
        PreposeConfig Support;
    };

    struct ObjectPoseInfo
    {
        std::string dataset;
        std::string name;
        Eigen::Matrix4f globalPose;
        Eigen::Vector3f localCenter;
        Eigen::Vector3f localExtend1;
        Eigen::Vector3f localExtend2;
        Eigen::Vector3f localExtend3;

        std::string package;
        std::string simoxFile;

        std::string attachedTo;
        Eigen::Matrix4f attachedToTransformation;
    };

    struct ControlInfo
    {
        Action action = Action_None;
        std::string side;
        ObjectPoseInfo objectPose;
        grasping::GraspCandidatePtr grasp;
        Eigen::Vector3f placePositionGlobal = Eigen::Vector3f::Zero();
        Eigen::Vector3f supportPositionOffset = Eigen::Vector3f::Zero();
    };

    struct PickAndPlaceComponent
        : virtual Component
        , virtual LightweightRemoteGuiComponentPluginUser
        , virtual RobotStateComponentPluginUser
        , virtual ArVizComponentPluginUser
        , virtual ObjectPoseClientPluginUser
    {
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            armarx::PropertyDefinitionsPtr properties(new armarx::ComponentPropertyDefinitions(getConfigIdentifier()));

            properties->defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver",
                    "Name of the topic the DebugObserver listens to.");
            properties->defineOptionalProperty<std::string>("ConfigPath", "InteractiveSceneExplorationX/PickAndPlaceComponent.json",
                    "Path to the config file");
            properties->defineOptionalProperty<std::string>("PlanPath", "InteractiveSceneExplorationX/PickAndPlacePlan.json",
                    "Path to the plan file");
            properties->defineOptionalProperty<std::string>("KinematicUnitName", "Armar6KinematicUnit", "Kinematic unit (duh)");
            properties->defineOptionalProperty<std::string>("RobotUnitName", "Armar6Unit", "Robot unit (duh)");
            properties->defineOptionalProperty<std::string>("RobotUnitObserverName", "RobotUnitObserver", "Robot unit observer (duh)");
            properties->defineOptionalProperty<std::string>("ForceTorqueObserverName", "Armar6ForceTorqueObserver", "Force torque observer observer (duh)");

            return properties;
        }


        std::string getDefaultName() const override
        {
            return "PickAndPlaceComponent";
        }

        void onInitComponent() override;

        void onConnectComponent() override;

        void onDisconnectComponent() override;

        void onExitComponent() override;

        void createRemoteGuiTab();
        void RemoteGui_update() override;

        void saveObjectPoses();
        void calculateGraspCandidates(ControlInfo const& control);
        void visualizeGraspCandidates();
        void visualizePlacePosition();
        void visualizeSupportPosition();
        void visualizeObjectPoses();
        void attachObjectToRobotPose(ObjectPoseInfo const& object, VirtualRobot::RobotNodePtr node);
        void detachAllObjects();

        void loadPlan();
        void executeNextPlanStep();

        void runControlLoop();

        void executeRetreat(ControlInfo const& control);
        void executePrepose(ControlInfo const& control);
        void executePreposeReverse(ControlInfo const& control);
        void executePush();
        void executePick(ControlInfo const& control);
        void executePlace(ControlInfo const& control);
        void executeSupportPrepose(ControlInfo const& control);
        void executeSupportPreposeReverse(ControlInfo const& control);
        void executeSupport(ControlInfo const& control);
        void executeSupportRetreat(ControlInfo const& control);

    private:
        DebugObserverInterfacePrx debugObserver;
        KinematicUnitInterfacePrx kinematicUnit;
        RobotUnitInterfacePrx robotUnit;
        ObserverInterfacePrx robotUnitObserver;
        ForceTorqueUnitObserverInterfacePrx forceTorqueObserver;

        objpose::data::ObjectPoseSeq objectPoses;
        std::vector<ObjectPoseInfo> objectPosesInfo;
        grasping::GraspCandidateSeq graspCandidates;
        std::string graspSide;
        Eigen::Matrix4f graspTcpToHandRoot;
        Eigen::Matrix4f graspRobotPoseGlobal;

        std::string planPath;
        Plan plan;

        PickAndPlaceConfig config;

        bool currentlyExecutingAction = false;
        PickAndPlaceTab tab;
        // UI state variables
        Action guiAction = Action_None;
        Side guiSide = Side_Right;
        int guiObjectIndex = 0;
        int guiGraspIndex = 0;
        int guiPlanStepIndex = 0;
        bool guiAutoUpdateGrasps = false;
        bool guiAutoUpdatePlace = false;
        Eigen::Vector3f guiPlacePosition = Eigen::Vector3f::Zero();
        Eigen::Vector3f guiSupportPosition = Eigen::Vector3f::Zero();
        bool recreateTabNextLoop = false;

        std::atomic<bool> runSimulation = false;
        std::atomic<bool> runExecution = false;
        std::atomic<bool> stop = false;

        // Control state variables
        std::mutex controlMutex;
        ControlInfo controlInfo;


        RunningTask<PickAndPlaceComponent>::pointer_type controlTask;

        VirtualRobot::RobotPtr robot;
        VirtualRobot::RobotPtr guiRobot;
        RobotNameHelperPtr robotNameHelper;
        std::map<std::string, Armar6GraspTrajectoryPtr> graspTrajectories;

    };
}

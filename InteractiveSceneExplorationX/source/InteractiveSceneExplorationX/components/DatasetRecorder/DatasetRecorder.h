/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::DatasetRecorder
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/components/pointcloud_core/PointCloudAndImageProcessor.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <ArmarXGui/libraries/RemoteGui/Client/EigenWidgets.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/Component.h>

#include <filesystem>
#include <mutex>

namespace armarx
{
    struct DatasetRecorderParams
    {
        std::filesystem::path DatasetPath;
        std::string InputProviderName;
        std::string InputSegmentProviderName;
        std::string ResultProviderName;
        std::string ResultSegmentProviderName;

        std::string CalibrationRobotNode;
        float CalibrationOffset;
    };

    using namespace armarx::RemoteGui::Client;
    struct DatasetRecorderTab : Tab
    {
        ToggleButton freezeInput;
        Button connectSegmenter;
        Button saveData;
        IntSpinBox saveIndex;
        Vector3Widget cropMin;
        Vector3Widget cropMax;
        FloatSpinBox calibrationOffset;
    };

    struct DatasetRecorderTabState
    {
        bool freezeInput = false;
        bool segmenterConnected = false;
        bool saveData = false;
        int saveIndex = 0;
        bool setSaveIndex = false;
        Eigen::Vector3f cropMin = Eigen::Vector3f(-1000.0f, -1000.0f, 900.0f);
        Eigen::Vector3f cropMax = Eigen::Vector3f(6000.0f, 5200.0f, 2000.0f);
        float calibrationOffset;
    };

    class DatasetRecorderPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DatasetRecorderPropertyDefinitions(std::string prefix);
    };


    struct DatasetRecorder
        : virtual visionx::PointCloudAndImageProcessor
        , virtual armarx::LightweightRemoteGuiComponentPluginUser
        , virtual armarx::ArVizComponentPluginUser
        , virtual armarx::RobotStateComponentPluginUser
    {
        std::string getDefaultName() const override;

        void onInitPointCloudAndImageProcessor() override;
        void onConnectPointCloudAndImageProcessor() override;
        void onDisconnectPointCloudAndImageProcessor() override;
        void onExitPointCloudAndImageProcessor() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void process() override;

        void createRemoteGuiTab();
        void RemoteGui_update() override;


        DatasetRecorderParams params;

        std::string referenceFrameName = "root";
        DatasetRecorderTab tab;
        VirtualRobot::RobotPtr robot;

        using PointCloud = pcl::PointCloud<pcl::PointXYZRGBA>;
        PointCloud::Ptr inputPointCloud;
        PointCloud::Ptr resultPointCloud;
        using LabeledPointCloud = pcl::PointCloud<pcl::PointXYZRGBL>;
        LabeledPointCloud::Ptr inputSegmentPointCloud;
        LabeledPointCloud::Ptr resultSegmentPointCloud;

        CByteImage* inputImages[2];
        CByteImage* resultImages[2];

        std::mutex tabStateMutex;
        DatasetRecorderTabState tabState;

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;

    };
}

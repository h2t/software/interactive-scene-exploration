/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::DatasetRecorder
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DatasetRecorder.h"

#include <VisionX/tools/ImageUtil.h>

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <SimoxUtility/json/json.hpp>
#include <SimoxUtility/json/eigen_conversion.h>
#include <SimoxUtility/json/io.h>

#include <pcl/io/pcd_io.h>


namespace armarx
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(DatasetRecorder);

    DatasetRecorderPropertyDefinitions::DatasetRecorderPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver",
                                            "Name of the topic the DebugObserver listens to.");

        defineOptionalProperty<std::string>("DatasetPath", "InteractiveSceneExplorationX/dataset", "Path to save the dataset to");

        defineOptionalProperty<std::string>("InputProviderName", "OpenNIPointCloudProvider", "Name of the image provider that should be used");
        defineOptionalProperty<std::string>("InputSegmentProviderName", "UserAssistedSegmenterResult", "Name of the segmentation provider to be used");
        defineOptionalProperty<std::string>("ResultProviderName", "DatasetRecorderResult", "Name of the image and point cloud result provider");
        defineOptionalProperty<std::string>("ResultSegmentProviderName", "DatasetRecorderSegmentResult", "Name of the image and point cloud result provider");

        defineOptionalProperty<std::string>("Calibration.RobotNode", "Neck_2_Pitch", "Robot node which can be calibrated");
        defineOptionalProperty<float>("Calibration.Offset", -0.01, "Offset for the node to be calibrated");
    }


    std::string DatasetRecorder::getDefaultName() const
    {
        return "DatasetRecorder";
    }


    void DatasetRecorder::onInitPointCloudAndImageProcessor()
    {
        offeringTopicFromProperty("DebugObserverName");

        std::string relativeDatasetPath = getProperty<std::string>("DatasetPath").getValue();
        std::string absoluteDatasetPath = relativeDatasetPath;
        if (!ArmarXDataPath::getAbsolutePath(relativeDatasetPath, absoluteDatasetPath))
        {
            ARMARX_WARNING << "Could not get absolute path from relative path: " << relativeDatasetPath;
        }
        params.DatasetPath = absoluteDatasetPath;


        params.InputProviderName = getProperty<std::string>("InputProviderName").getValue();
        params.InputSegmentProviderName = getProperty<std::string>("InputSegmentProviderName").getValue();
        params.ResultProviderName = getProperty<std::string>("ResultProviderName").getValue();
        params.ResultSegmentProviderName = getProperty<std::string>("ResultSegmentProviderName").getValue();
        params.CalibrationRobotNode = getProperty<std::string>("Calibration.RobotNode").getValue();
        params.CalibrationOffset = getProperty<float>("Calibration.Offset").getValue();
        tabState.calibrationOffset = params.CalibrationOffset;

        usingImageProvider(params.InputProviderName);
        usingPointCloudProvider(params.InputProviderName);
    }


    void DatasetRecorder::onConnectPointCloudAndImageProcessor()
    {
        getTopicFromProperty(debugObserver, "DebugObserverName");

        {
            visionx::ImageProviderInfo imageProviderInfo = getImageProvider(params.InputProviderName);

            referenceFrameName = "root";
            if (auto refFrameProvider = visionx::ReferenceFrameInterfacePrx::checkedCast(imageProviderInfo.proxy))
            {
                referenceFrameName = refFrameProvider->getReferenceFrame();
            }
            ARMARX_INFO << "Using reference frame: " << referenceFrameName;

            if (imageProviderInfo.numberImages != 2)
            {
                ARMARX_WARNING << "Expected 2 input images (RGB and depth)";
                throw armarx::LocalException() << "Expected 2 input images (RGB and depth), "
                                               << "but got: " << imageProviderInfo.numberImages;
            }

            for (int i = 0 ; i < 2; i++)
            {
                inputImages[i] = visionx::tools::createByteImage(imageProviderInfo);
                resultImages[i] = inputImages[i];
            }

            visionx::ImageDimension dimension(resultImages[0]->width, resultImages[0]->height);

            int pointCount = dimension.width * dimension.height;
            inputPointCloud.reset(new PointCloud);
            inputPointCloud->points.reserve(pointCount);
            resultPointCloud.reset(new PointCloud);
            resultPointCloud->points.reserve(pointCount);
            inputSegmentPointCloud.reset(new LabeledPointCloud);
            inputSegmentPointCloud->points.reserve(pointCount);
            resultSegmentPointCloud.reset(new LabeledPointCloud);
            resultSegmentPointCloud->points.reserve(pointCount);

            enableResultImagesAndPointClouds<pcl::PointXYZRGBA>(
                params.ResultProviderName, 2, dimension, visionx::eRgb);

            // TODO: Only if after we are already connected!
            //enableResultPointCloudForInputProvider<pcl::PointXYZRGBL>(params.InputSegmentProviderName, params.ResultSegmentProviderName);
        }

        robot = addRobot("Armar6", VirtualRobot::RobotIO::eStructure);

        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }


    void DatasetRecorder::onDisconnectPointCloudAndImageProcessor()
    {

    }


    void DatasetRecorder::onExitPointCloudAndImageProcessor()
    {

    }


    void DatasetRecorder::process()
    {
        DatasetRecorderTabState state;
        {
            std::unique_lock lock(tabStateMutex);
            state = this->tabState;
        }

        if (state.freezeInput)
        {
            // The input is frozen, so we just do not update the current values
        }
        else
        {
            if (!waitForImages(params.InputProviderName))
            {
                ARMARX_WARNING << "Timeout while waiting for images";
                return;
            }

            if (!waitForPointClouds(params.InputProviderName))
            {
                ARMARX_WARNING << "Timeout while waiting for pointcloud";
                return;
            }

            if (getImages(inputImages) != 2)
            {
                ARMARX_WARNING << "Unable to transfer or read images";
                return;
            }

            if (!getPointClouds(params.InputProviderName, inputPointCloud))
            {
                ARMARX_WARNING << "Unable to transfer or read point cloud";
                return;
            }

            synchronizeLocalClone(robot);
            if (robot->hasRobotNode(params.CalibrationRobotNode))
            {
                auto node = robot->getRobotNode(params.CalibrationRobotNode);
                float realJointValue = node->getJointValue();
                node->setJointValue(realJointValue + tabState.calibrationOffset);
            }

            // Calculate the result point cloud
            // Transform to global and crop
            Eigen::Matrix4f referenceGlobalPose = robot->getRobotNode(referenceFrameName)->getGlobalPose();
            Eigen::Affine3f toGlobal(referenceGlobalPose);
            resultPointCloud->resize(inputPointCloud->size());
            auto* inputPoints = inputPointCloud->points.data();
            auto* resultPoints = resultPointCloud->points.data();
            int pointsSize = (int)resultPointCloud->size();
            int resultIndex = 0;
            Eigen::Vector3f cropMin = state.cropMin;
            Eigen::Vector3f cropMax = state.cropMax;
            for (int i = 0; i < pointsSize; ++i)
            {
                auto& inputPoint = inputPoints[i];
                Eigen::Vector3f localPoint = inputPoint.getVector3fMap();
                Eigen::Vector3f globalPoint = toGlobal * localPoint;
                if (true && globalPoint.x() >= cropMin.x()
                    && globalPoint.x() <= cropMax.x()
                    && globalPoint.y() >= cropMin.y()
                    && globalPoint.y() <= cropMax.y()
                    && globalPoint.z() >= cropMin.z()
                    && globalPoint.z() <= cropMax.z())
                {
                    auto& resultPoint = resultPoints[resultIndex];
                    resultPoint.getVector3fMap() = globalPoint;
                    resultPoint.r = inputPoint.r;
                    resultPoint.g = inputPoint.g;
                    resultPoint.b = inputPoint.b;
                    resultPoint.a = inputPoint.a;
                    resultIndex += 1;
                }
            }
            resultPointCloud->resize(resultIndex);
        }

        if (state.segmenterConnected)
        {
            // We are connected to the segmenter
            if (!waitForPointClouds(params.InputSegmentProviderName))
            {
                ARMARX_WARNING << "Timeout while waiting for segmented pointcloud";
                return;
            }

            if (!getPointClouds(params.InputSegmentProviderName, inputSegmentPointCloud))
            {
                ARMARX_WARNING << "Unable to transfer or read segmented point cloud";
                return;
            }

            *resultSegmentPointCloud = *inputSegmentPointCloud;
        }

        ARMARX_IMPORTANT << "Got images and point cloud";

        std::filesystem::path savePath;
        if (state.saveData)
        {
            std::string saveIndexStr = std::to_string(state.saveIndex);
            if (saveIndexStr.size() == 1)
            {
                saveIndexStr = "0" + saveIndexStr;
            }
            savePath = params.DatasetPath / saveIndexStr;
            std::filesystem::create_directory(savePath);

            // Save the input image and point cloud to the directory
            ARMARX_IMPORTANT << "Trying to save to directory: " << savePath;

            std::filesystem::path originalPath = savePath / "original.pcd";
            pcl::io::savePCDFile<pcl::PointXYZRGBA>(originalPath.string(), *inputPointCloud);

            std::filesystem::path globalPath = savePath / "global.pcd";
            pcl::io::savePCDFile<pcl::PointXYZRGBA>(globalPath.string(), *resultPointCloud);

            std::filesystem::path labeledPath = savePath / "labeled.pcd";
            pcl::io::savePCDFile<pcl::PointXYZRGBL>(labeledPath.string(), *resultSegmentPointCloud);

            {
                nlohmann::json data;
                auto& robotData = data["robot"];
                robotData["global_pose"] = robot->getGlobalPose();
                robotData["joint_configuration"] = robot->getConfig()->getRobotNodeJointValueMap();
                auto& referenceData = data["camera"];
                referenceData["frame_name"] = referenceFrameName;
                referenceData["global_pose"] = robot->getRobotNode(referenceFrameName)->getGlobalPose();

                std::filesystem::path robotPath = savePath / "robot-state.json";
                nlohmann::write_json(robotPath.string(), data, 2);
            }

            std::filesystem::path rgbPath = savePath / "rgb.bmp";
            inputImages[0]->SaveToFile(rgbPath.string().c_str());
            std::filesystem::path depthPath = savePath / "depth.bmp";
            inputImages[1]->SaveToFile(depthPath.string().c_str());

            std::unique_lock lock(tabStateMutex);
            tabState.saveData = false;
            tabState.saveIndex += 1;
            tabState.setSaveIndex = true;
        }

        for (int i = 0 ; i < 2; i++)
        {
            resultImages[i] = inputImages[i];
        }

        provideResultImages(resultImages);
        provideResultPointClouds(params.ResultProviderName, resultPointCloud);
        if (state.segmenterConnected)
        {
            provideResultPointClouds(params.ResultSegmentProviderName, resultSegmentPointCloud);
        }
    }

    void DatasetRecorder::createRemoteGuiTab()
    {
        GridLayout grid;
        int row = 0;
        {
            tab.freezeInput.setLabel("Freeze Input");
            tab.freezeInput.setValue(tabState.freezeInput);
            grid.add(tab.freezeInput, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            tab.connectSegmenter.setLabel("Connect Segmenter");
            grid.add(tab.connectSegmenter, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            tab.saveData.setLabel("Save Data");
            grid.add(tab.saveData, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            tab.saveIndex.setValue(tabState.saveIndex);
            tab.saveIndex.setRange(0, 99);
            grid.add(Label("Save Index:"), Pos{row, 0});
            grid.add(tab.saveIndex, Pos{row, 1});
            row += 1;
        }
        {
            tab.cropMin.setValue(tabState.cropMin);
            tab.cropMin.setRange(-10000.0f, 10000.0f);
            tab.cropMin.setSteps(1000);
            tab.cropMin.setDecimals(0);
            grid.add(Label("Crop Min:"), Pos{row, 0});
            grid.add(tab.cropMin, Pos{row, 1});
            row += 1;
        }
        {
            tab.cropMax.setValue(tabState.cropMax);
            tab.cropMax.setRange(-10000.0f, 10000.0f);
            tab.cropMax.setSteps(1000);
            tab.cropMax.setDecimals(0);
            grid.add(Label("Crop Max:"), Pos{row, 0});
            grid.add(tab.cropMax, Pos{row, 1});
            row += 1;
        }
        {
            tab.calibrationOffset.setRange(-0.20, 0.20);
            tab.calibrationOffset.setSteps(80);
            tab.calibrationOffset.setValue(tabState.calibrationOffset);
            tab.calibrationOffset.setDecimals(3);
            grid.add(Label("Calibration Offset:"), Pos{row, 0});
            grid.add(tab.calibrationOffset, Pos{row, 1});
            row += 1;
        }

        RemoteGui_createTab("DatasetRecorder", grid, &tab);
    }

    void DatasetRecorder::RemoteGui_update()
    {
        {
            std::unique_lock lock(tabStateMutex);
            tabState.freezeInput = tab.freezeInput.getValue();
            if (tabState.setSaveIndex)
            {
                tab.saveIndex.setValue(tabState.saveIndex);
                tabState.setSaveIndex = false;
            }
            else
            {
                tabState.saveIndex = tab.saveIndex.getValue();
            }
            tabState.cropMin = tab.cropMin.getValue();
            tabState.cropMax = tab.cropMax.getValue();
            tabState.calibrationOffset = tab.calibrationOffset.getValue();
            if (tab.saveData.wasClicked())
            {
                tabState.saveData = true;
            }
            if (tab.connectSegmenter.wasClicked())
            {
                usingPointCloudProvider(params.InputSegmentProviderName);
                getPointCloudProvider(params.InputSegmentProviderName);
                enableResultPointCloudForInputProvider<pcl::PointXYZRGBL>(
                    params.InputSegmentProviderName, params.ResultSegmentProviderName);

                tabState.segmenterConnected = true;
            }
        }
    }

    armarx::PropertyDefinitionsPtr DatasetRecorder::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new DatasetRecorderPropertyDefinitions(
                getConfigIdentifier()));
    }
}

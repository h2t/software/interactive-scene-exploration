
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore RansacUncertainty)
 
armarx_add_test(RansacUncertaintyTest RansacUncertaintyTest.cpp "${LIBS}")

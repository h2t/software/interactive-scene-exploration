#!/bin/bash

cd $HOME
git clone https://gitlab.com/h2t/interactive-scene-exploration.git
cd interactive-scene-exploration
git checkout iser-2020

cd InteractiveSceneExploration/build
CXX=g++-8 cmake .. -DCMAKE_BUILD_TYPE=Release
make -j8

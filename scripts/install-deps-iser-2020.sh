#!/bin/bash

# Build tools
sudo apt-get install --yes git cmake build-essential g++-8

# Install library dependencies
sudo apt-get install --yes libbullet-dev libboost-dev libeigen3-dev libpcl-dev qtbase5-dev freeglut3-dev

# Build additional library dependencies

# Simox
cd $HOME
git clone https://gitlab.com/Simox/simox.git
cd simox/build
git checkout iser-2020
CXX=g++-8 cmake .. -DCMAKE_BUILD_TYPE=Release -DSimox_BUILD_VirtualRobot=OFF -DSimox_BUILD_Saba=OFF -DSimox_BUILD_SimDynamics=OFF -DSimox_BUILD_GraspStudio=OFF
make -j8

# SemanticObjectRelations
cd $HOME
git clone https://gitlab.com/h2t/semantic-object-relations.git
cd semantic-object-relations/SemanticObjectRelations/
git checkout iser-2020
mkdir build
cd build
CXX=g++-8 cmake .. -DCMAKE_BUILD_TYPE=Release
make -j8
